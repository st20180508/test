var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_PATH: '"/"',
  PREFIX_CUT: `"${process.env.PREFIX_CUT}"`,
  OAUTH: `"${process.env.OAUTH}"`
})
