import { buildModule } from '@/store/module'
// import { rest } from '@/util'

const { types, module } = buildModule({
  name: 'share',
  restUrl: '/sns/shares',
  types: {
    M_CHANGE_SHARE_FORM_SHOW: 'M_CHANGE_SHARE_FORM_SHOW',
    M_CHANGE_SHARE_FORM_DATA: 'M_CHANGE_SHARE_FORM_DATA',
    M_CHANGE_SHARE_QUOTE_DATA: 'M_CHANGE_SHARE_QUOTE_DATA'
  },
  state: {
    shareFormStatus: false,
    shareFormData: {
      from: '',
      type: '',
      item: {},
      shareParent: true
    },
    quotes: {}
  }
})

Object.assign(module.mutations, {
  [ types.M_CHANGE_SHARE_FORM_SHOW ] (state, bool) {
    state.shareFormStatus = bool
  },
  [ types.M_CHANGE_SHARE_FORM_DATA ] (state, {from, type, item, shareParent}) {
    state.shareFormData.from = from
    state.shareFormData.type = type
    state.shareFormData.item = item
    state.shareFormData.shareParent = shareParent
  },
  [ types.M_CHANGE_SHARE_QUOTE_DATA ] (state, data) {
    state.quotes[data.id] = data
  }
})

Object.assign(module.actions, {
})

export { types }
export default module
export const shareFormShow = {
  get () {
    return this.$store.state.share.shareFormStatus
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_SHARE_FORM_SHOW, val)
  }
}
export const shareData = {
  get () {
    return this.$store.state.share.shareFormData
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_SHARE_FORM_DATA, val)
  }
}

export function shareRouteTo (feed) {
  let r
  let objContent = feed.objContent || {}
  if (feed.objType === 'POST' && (objContent.model === 'blog' || objContent.model === 'reprint')) {
    r = {name: 'article', params: {id: feed.objId}}
  } else if (feed.objType === 'POST' && objContent.model === 'activity') {
    r = {name: 'activity', params: {activityId: objContent.bindId}}
  } else if (feed.groupId) {
    r = {name: 'groupOneFeed', params: {feedId: feed.id, groupId: feed.groupId}}
  } else {
    r = {name: 'userOneFeed', params: {feedId: feed.id, userId: feed.userId}}
  }
  return r
}

export const quotesData = {
  get () {
    return this.$store.state.share.quotes
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_SHARE_QUOTE_DATA, val)
  }
}
