import { buildModule } from '@/store/module'

const { types, module } = buildModule({
  name: 'post',
  restUrl: '/sns/posts',
  restCreateUrl: '/sns/posts/normal',
  restListQuery: 'sort=id,asc',
  types: {
    M_SHOW_ITEM_CREATE_PANEL: 'M_SHOW_ITEM_CREATE_PANEL',
    M_CHANGE_BLOG_ITEM: 'M_CHANGE_BLOG_ITEM',
    M_SHOW_BLOG_ITEM_PANEL: 'M_SHOW_BLOG_ITEM_PANEL',
    M_SHOW_BLOG_ITEM_CREATE_PANEL: 'M_SHOW_BLOG_ITEM_CREATE_PANEL'
  },
  state: {
    showItemCreatePanel: false,
    blogItem: {},
    showBlogItemPanel: false,
    showBlogItemCreatePanel: false,
    objType: {
      group: 101,
      user: 102,
      post: 0,
      image: 1,
      video: 2,
      document: 3,
      file: 4,
      activity: 5,
      blog: 6,
      vote: 7,
      comment: 8,
      task: 9,
      topic: 10,
      question: 11,
      goods: 12,
      reprint: 13
    },
    objTypeName: {
      101: 'group',
      102: 'user',
      0: 'post',
      1: 'image',
      2: 'video',
      3: 'document',
      4: 'file',
      5: 'activity',
      6: 'blog',
      7: 'vote',
      8: 'comment',
      9: 'task',
      10: 'topic',
      11: 'question',
      12: 'goods',
      13: 'reprint'
    }
  }
})

Object.assign(module.mutations, {
  [ types.M_SHOW_ITEM_CREATE_PANEL ] (state, bool) {
    state.showItemCreatePanel = bool
  },
  [ types.M_CHANGE_BLOG_ITEM ] (state, data) {
    state.blogItem = data
  },
  [ types.M_SHOW_BLOG_ITEM_PANEL ] (state, bool) {
    state.showBlogItemPanel = bool
  },
  [ types.M_SHOW_BLOG_ITEM_CREATE_PANEL ] (state, bool) {
    state.showBlogItemCreatePanel = bool
  }
})

export { types }
export default module

export const currBlogItem = {
  get () {
    return this.$store.state.post.blogItem
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_BLOG_ITEM, val)
  }
}

export const blogFormShow = {
  get () {
    return this.$store.state.post.showBlogItemPanel
  },
  set (val) {
    this.$store.commit(types.M_SHOW_BLOG_ITEM_PANEL, val)
  }
}

export const blogFormShowCreate = {
  get () {
    return this.$store.state.post.showBlogItemCreatePanel
  },
  set (val) {
    this.$store.commit(types.M_SHOW_BLOG_ITEM_CREATE_PANEL, val)
  }
}

export const objTypes = {
  get () {
    return this.$store.state.post.objType
  }
}
