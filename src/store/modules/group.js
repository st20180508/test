import { buildModule } from '@/store/module'
import { buildUrl, rest } from '@/util'

const {types, module} = buildModule({
  name: 'group',
  restUrl: '/sns/groups',
  restListQuery: 'sort=id,asc',
  types: {
    M_CHANGE_CURRENT_GROUPREF: 'M_CHANGE_CURRENT_GROUPREF',
    A_GET_CURRENT_GROUPREF: 'A_GET_CURRENT_GROUPREF',
    A_CHANGE_FOLLOW_GROUP: 'A_CHANGE_FOLLOW_GROUP',
    A_CHANGE_GROUP_NOTIFY: 'A_CHANGE_GROUP_NOTIFY',
    M_SHOW_ITEM_CREATE_PANEL: 'M_SHOW_ITEM_CREATE_PANEL',
    M_CHANGE_GROUPS_RELOAD_STATUS: 'M_CHANGE_GROUPS_RELOAD_STATUS',
    A_GET_TYPE_GROUPS_LIST: 'A_GET_TYPE_GROUPS_LIST',
    M_SHOW_GROUP_MANAGEMENT: 'M_SHOW_GROUP_MANAGEMENT',
    M_CHANGE_REQUEST_TYPE_COUNTS: 'M_CHANGE_REQUEST_TYPE_COUNTS',
    M_CHANGE_GROUP_MANAGER_ACTIVE_NAME: 'M_CHANGE_GROUP_MANAGER_ACTIVE_NAME',
    M_CHANGE_GROUP_MANAGER_SHOW: 'M_CHANGE_GROUP_MANAGER_SHOW',
    M_SET_CURRREF_STATUS: 'M_SET_CURRREF_STATUS'
  },
  state: {
    restGetUrl: '/sns/groups/{{id}}/refs',
    currItemRef: {},
    currRefIsManager: false,
    currRefIsSuperman: false,
    showItemCreatePanel: false,
    reloadGroups: false,
    groupValueTypes: {
      0: '组织机构',
      1: '团队和项目',
      2: '社交和其他',
      3: '开放讨论',
      4: '交易中心',
      5: '新闻公告'
    },
    groupKeyTypes: {
      organization: '组织机构',
      team_and_project: '团队和项目',
      social_networking: '社交和其他',
      open_discussion: '开放讨论',
      trading_center: '交易中心',
      news_notice: '新闻公告'
    },
    requestTypeCounts: null,
    groupManager: {
      show: false,
      activeName: ''
    }
  }
})

Object.assign(module.mutations, {
  [ types.M_CHANGE_CURRENT_GROUPREF ] (state, vo) {
    if (vo.group.id === state.currItem.id) {
      state.currItemRef = {...state.currItemRef, ...vo}
    } else {
      state.currItemRef = vo
    }
  },
  [ types.M_SHOW_ITEM_CREATE_PANEL ] (state, bool) {
    state.showItemCreatePanel = bool
  },
  [ types.M_CHANGE_GROUPS_RELOAD_STATUS ] (state, bool) {
    state.reloadGroups = bool
  },
  [ types.M_SHOW_GROUP_MANAGEMENT ] (state, bool) {
    state.showGroupManagement = bool
  },
  [ types.M_CHANGE_REQUEST_TYPE_COUNTS ] (state, data) {
    state.requestTypeCounts = data
  },
  [ types.M_CHANGE_GROUP_MANAGER_ACTIVE_NAME ] (state, activeName) {
    state.groupManager.activeName = activeName
  },
  [ types.M_CHANGE_GROUP_MANAGER_SHOW ] (state, show) {
    state.groupManager.show = show
  },
  [ types.M_SET_CURRREF_STATUS ] (state, groupMember) {
    if (groupMember && groupMember.id) {
      state.currRefIsManager = groupMember.type === 3
      state.currRefIsSuperman = groupMember.type === 2
    }
  }
})

Object.assign(module.actions, {
  [ types.A_GET_TYPE_GROUPS_LIST ] ({dispatch, commit, state}, type) {
    return rest.get('/sns/groups/users/joined')
  },
  [ types.A_GET_CURRENT_GROUPREF ] ({ commit, state }, id) {
    return rest.get(buildUrl(state.restGetUrl, {id}))
      .then((json) => {
        commit(types.ON_FETCH, false)
        commit(types.M_CHANGE_CURRENT_GROUPREF, JSON.parse(JSON.stringify(json)))
        commit(types.M_CHANGE_CURRENT, JSON.parse(JSON.stringify(json.group)))
        commit(types.M_SET_CURRREF_STATUS, json.groupMember)
        return json
      })
      .catch(e => {
        commit(types.ON_FETCH, false)
        return Promise.reject(e)
      })
  },
  [ types.A_CHANGE_FOLLOW_GROUP ] ({ commit, state }, params) {
    return rest.create('/sns/groups/follow', params)
  },
  [ types.A_CHANGE_GROUP_NOTIFY ] ({ commit, state }, params) {
    return rest.create('/sns/groups/notify', params)
  }
})
export {types}
export default module

export const groupManagementShow = {
  get () {
    return this.$store.state.group.groupManager.show
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_GROUP_MANAGER_SHOW, val)
  }
}

export const groupManagementActiveName = {
  get () {
    return this.$store.state.group.groupManager.activeName
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_GROUP_MANAGER_ACTIVE_NAME, val)
  }
}
