import { buildModule } from '@/store/module'
import { rest } from '@/util'

const Api = {
  GET_ARTICLE: '/hdds/item/get_0/{biz}/{iid}',
  GET_USER_INFO: '/hdds/user/get/headlines/{uid}',
  GET_USER_CONFIG: '/hdds/user/getConfig/headlines/{uid}/{pid}',
  LIST_ARTICLE: '/hdds/item/{biz}/{site}/{catalog}/{lastPage}',
  RECOMMEND_ARTICLE: '/hdds/taste/rmdByUser/headlines/ug/{uid}/{dir}',
  SAVE_CONFIG: '/hdds/user/save/headlines/{uid}',
  RECOMMEND_ARTICLE_BY_ARTICLE: '/hdds/taste/rmdByItem/{iid}/{uid}',
  RECOMMEND_ARTICLE_BY_TAG: '/hdds/taste/rmdByT/headlines/g/6',
  GET_SITE_MSG: '/hdds/item/catalogTree/{site}',
  GET_ARTICLE_BY_SITE: '/cms/wNewsRecommend.sp?act=search&site={site}&rows={hm}&flimit=200&n={pageNum}',
  GET_ARTICLE_BY_CATAGORY: '/cms/wNewsRecommend.sp?act=search&site={site}&catalog={key}&rows={hm}&flimit=200&n={pageNum}',
  GET_ARTICLE_WITH_KEY_SEARCH: '/cms/wNewsRecommend.sp?act=search&site={site}&q={key}&rows={hm}&flimit=200&n={pageNum}',
  COLLECT_ARTICLE: '/hdds/bvlg//bhv/clt/headlines/{uid}/{iid}/{type}',
  GET_ARTCILE_FROM_XY_SITE: '/cms/wNewsRecommend.sp?act=searchRange&site={site}&latalng={xy}&range={range}'
}

function post (url, param) {
  return rest.post(url, param).then(function (res) {
    return res
  })
}
function changeToVo (article) {
  var ar = {}
  ar.title = article.title[0].value
  ar.source = article.source[0].value
  ar.id = article.id[0].value
  ar.caName = article.curCatalog[0].convertedValue
  ar.caId = article.curCatalog[0].value
  ar.pubDate = article.pubDate[0].value
  ar.author = article.author[0].value
  ar.arThumb = article.arThumb[0].value
  return ar
}
function changeSimpleToVo (article) {
  var ar = {}
  ar.title = article.title[0].value
  ar.id = article.id[0].value
  ar.pubDate = article.pubDate[0].value
  ar.author = article.author[0].value
  ar.latalng = article.latalng[0].value
  return ar
}

function get (url) {
  return rest.get(url).then(function (res) {
    return res
  })
}

const { types, module } = buildModule({
  name: 'news',
  restUrl: '/hdds/users',
  restListQuery: 'deptRole={{deptId}}&sort=id,asc',
  types: {
    NEWS_GET_ARTICLE: 'NEWS_GET_ARTICLE',
    NEWS_GET_USER_INFO: 'NEWS_GET_USER_INFO',
    NEWS_GET_USER_CONFIG: 'NEWS_GET_USER_CONFIG',
    NEWS_SAVE_USER_INFO: 'NEWS_SAVE_USER_INFO',
    NEWS_LIST_ARTICLE: 'NEWS_LIST_ARTICLE',
    NEWS_RECOMMEND_ARTICLE: 'NEWS_RECOMMEND_ARTICLE',
    NEWS_GET_SITE_MSG: 'NEWS_GET_SITE_MSG',
    NEWS_GET_LEFT_CATALOG: 'NEWS_GET_LEFT_CATALOG',
    NEWS_RECOMMEND_BY_ARTICLE: 'NEWS_RECOMMEND_BY_ARTICLE',
    NEWS_SET_PAGE: 'NEWS_SET_PAGE',
    NEWS_SET_USER: 'NEWS_SET_USER',
    NEWS_SET_P_CONFIG: 'NEWS_SET_P_CONFIG',
    NEWS_SET_CONFIG: 'NEWS_SET_CONFIG',
    NEWS_SET_LIST_ARTICLE_1: 'NEWS_SET_LIST_ARTICLE_1',
    NEWS_ADD_LIST_ARTICLE_1: 'NEWS_ADD_LIST_ARTICLE_1',
    NEWS_SELECT_LIST_ARTICLE_1_INDEX: 'NEWS_SELECT_LIST_ARTICLE_1_INDEX',
    NEWS_SET_LIST_ARTICLE_2: 'NEWS_SET_LIST_ARTICLE_2',
    NEWS_SET_LIST_ARTICLE_3: 'NEWS_SET_LIST_ARTICLE_3',
    NEWS_SET_CURRENT_SITE: 'NEWS_SET_CURRENT_SITE',
    NEWS_RECOMMEND_ARTICLE_BY_TAG: 'NEWS_RECOMMEND_ARTICLE_BY_TAG',
    NEWS_RECOMMEND_ARTICLE_BY_TEMP_TAG: 'NEWS_RECOMMEND_ARTICLE_BY_TEMP_TAG',
    NEWS_RECOMMEND_ARTICLE_BY_SITE: 'NEWS_RECOMMEND_ARTICLE_BY_SITE',
    NEWS_RECOMMEND_ARTICLE_BY_CATAGORY: 'NEWS_RECOMMEND_ARTICLE_BY_CATAGORY',
    NEWS_SEARCH_WITH_KEY: 'NEWS_SEARCH_WITH_KEY',
    NEWS_INIT_PAGE_SET: 'NEWS_INIT_PAGE_SET',
    NEWS_SET_CURRENT_SEARCH: 'NEWS_SET_CURRENT_SEARCH',
    NEWS_COLLECT_ARTICLE: 'NEWS_COLLECT_ARTICLE',
    NEWS_GET_ARTICLE_BY_XY_SITE: 'NEWS_GET_ARTICLE_BY_XY_SITE',
    NEWS_SET_SHOW_CONFIG_VIEW: 'NEWS_SET_SHOW_CONFIG_VIEW',
    NEWS_SET_PERSONAL: 'NEWS_SET_PERSONAL',
    NEWS_SET_TEMP_GRAPH: 'NEWS_SET_TEMP_GRAPH'
  },
  state: {
    personal: null,
    showConfigView: false,
    user: {},
    article: {},
    listArticle1: [],
    listArticle2: [],
    listArticle3: [],
    tempGraph: {},
    pConfig: [],
    config: [],
    currPageNum: 1,
    pageSize: 20,
    currArticleIndex: 0,
    currNode: {pid: 'root', caId: 'rec', caName: '推荐'},
    currSearchNode: {type: 'site', key: 'rec'},
    defaultSite: [{name: '新闻', id: '190014', alias: 'headlines'},
      {name: '政务', id: '190019', alias: 'govheadlines'},
      {name: '职场', id: '', alias: 'headlines'},
      {name: '地方', id: '190014', alias: 'headlines'}]
  }
})

export function currConfig () {
  var compConfig = this.company.config
  if (compConfig == null) {
    return this.defaultSite
  }
  return compConfig
}

Object.assign(module.getters, {
})

Object.assign(module.mutations, {
  [ types.NEWS_SET_PAGE ]: (state, article) => {
    state.article = article
  },
  [ types.NEWS_SET_TEMP_GRAPH ]: (state, action) => {
    if (action.tags && action.time > 10000) {
      for (var k in state.tempGraph) {
        state.tempGraph[k] = state.tempGraph[k] * 0.9
      }
      for (var tag in action.tags) {
        if (state.tempGraph[tag]) {
          state.tempGraph[tag] = state.tempGraph[tag] + action.tags[tag]
        } else {
          state.tempGraph[tag] = action.tags[tag]
        }
      }
    }
  },
  [ types.NEWS_SET_USER ]: (state, user) => {
    state.config = user.info && user.info.config ? JSON.parse(user.info.config) : []
    state.user = user
  },
  [ types.NEWS_SET_CONFIG ]: (state, config) => {
    state.config = config
  },
  [ types.NEWS_SET_P_CONFIG ]: (state, config) => {
    state.pConfig = config
  },
  [ types.NEWS_SET_LIST_ARTICLE_1 ]: (state, articles) => {
    state.listArticle1 = articles
  },
  [ types.NEWS_ADD_LIST_ARTICLE_1 ]: (state, articles) => {
    for (var index in articles) {
      state.listArticle1.push(articles[index])
    }
  },
  [ types.NEWS_SELECT_LIST_ARTICLE_1_INDEX ]: (state, index) => {
    state.currArticleIndex = index
  },
  [ types.NEWS_SET_LIST_ARTICLE_2 ]: (state, articles) => {
    state.listArticle2 = articles
  },
  [ types.NEWS_SET_LIST_ARTICLE_3 ]: (state, articles) => {
    state.listArticle3 = articles
  },
  [ types.NEWS_SET_CURRENT_SITE ]: (state, node) => {
    state.currNode = node
  },
  [ types.NEWS_INIT_PAGE_SET ]: (state, page) => {
    state.currPageNum = page
  },
  [ types.NEWS_SET_CURRENT_SEARCH ]: (state, node) => {
    state.currSearchNode = node
  },
  [ types.NEWS_SET_SHOW_CONFIG_VIEW ]: (state, node) => {
    state.showConfigView = node
  },
  [ types.NEWS_SET_PERSONAL ]: (state, node) => {
    state.personal = node
  }
})

Object.assign(module.actions, {
  [ types.NEWS_GET_ARTICLE ]: ({commit, state}, param) => {
    var url = Api.GET_ARTICLE.replace('{biz}', param.biz).replace('{iid}', param.iid)
    get(url).then(function (res) {
      commit(types.NEWS_SET_PAGE, res)
    })
  },
  [ types.NEWS_GET_USER_INFO ]: ({commit, state}, uid) => {
    var url = Api.GET_USER_INFO.replace('{uid}', uid)
    get(url).then(function (res) {
      res.id = uid
      commit(types.NEWS_SET_USER, res)
    })
  },
  [ types.NEWS_GET_USER_CONFIG ]: ({commit, state}, uid) => {
    var url = Api.GET_USER_CONFIG.replace('{uid}', uid).replace('{pid}', this.user.info['pid'])
    get(url).then(function (res) {
      commit('NEWS_SET_P_CONFIG', res)
    })
  },
  [ types.NEWS_SAVE_USER_INFO ]: ({commit, state}, param) => {
    if (param.uid) {
      var url = Api.SAVE_CONFIG.replace('{uid}', param.uid)
      return post(url, param).then(function (res) {
        return res
      })
    } else {
      return 'fail'
    }
  },
  [ types.NEWS_LIST_ARTICLE ]: ({commit, state}, param) => {
    var url = Api.LIST_ARTICLE.replace('{biz}', param.biz).replace('{site}', param.site).replace('{catalog}', param.catalog)
    get(url).then(function (res) {
      commit(types.NEWS_SET_LIST_ARTICLE_1, res)
      return res
    })
  },
  [ types.NEWS_RECOMMEND_ARTICLE ]: ({commit, state}, param) => {
    var id = param.uid || state.user.id
    // hdds/taste/rmdByUser/headlines/ug/{uid}/{dir}
    var url = Api.RECOMMEND_ARTICLE.replace('{uid}', id).replace('{dir}', 'up')
    get(url).then(function (res) {
      if (param.loadMore) {
        commit(types.NEWS_ADD_LIST_ARTICLE_1, res)
      } else {
        commit(types.NEWS_SET_LIST_ARTICLE_1, res)
      }
      return res
    })
  },
  [ types.NEWS_GET_SITE_MSG ]: ({commit, state}, param) => {
    if (param.personal) {
      if (param.isAdmin) {
        // GET_USER_CONFIG: '/hdds/user/get/headlines/{uid}/{pid}',
        let url = Api.GET_USER_CONFIG.replace('{uid}', param.uid).replace('{pid}', 'all')
        get(url).then(function (res) {
          commit(types.NEWS_SET_P_CONFIG, res)
          get(Api.GET_USER_CONFIG.replace('{pid}', param.uid)).then(function (res) {
            commit(types.NEWS_SET_CONFIG, res)
          })
        })
      } else {
        let url = Api.GET_USER_CONFIG.replace('{uid}', param.uid).replace('{pid}', param.pid)
        get(url).then(function (res) {
          commit(types.NEWS_SET_P_CONFIG, res)
          get(Api.GET_USER_CONFIG.replace('{pid}', param.uid)).then(function (res1) {
            commit(types.NEWS_SET_CONFIG, res1)
          })
        })
      }
    } else {
      let url = Api.GET_USER_CONFIG.replace('{uid}', param.uid).replace('{pid}', 'all')
      return get(url).then(function (res) {
        commit(types.NEWS_SET_P_CONFIG, res)
        return get(Api.GET_USER_CONFIG.replace('{uid}', param.uid).replace('{pid}', param.pid)).then(function (res1) {
          commit(types.NEWS_SET_CONFIG, res1)
          return res1
        })
      })
    }
  },
  [ types.NEWS_GET_LEFT_CATALOG ]: ({commit, state}, site) => {
  },
  [ types.NEWS_RECOMMEND_ARTICLE_BY_TAG ]: ({commit, state}, tags) => {
    if (state.currNode.caId === '190019') {
      var url = Api.GET_ARTICLE_WITH_KEY_SEARCH.replace('{site}', '190019').replace('{key}', state.article.title).replace('{hm}', 20).replace('{pageNum}', 1)
      get(url).then(function (res) {
        if (res.result.articleList) {
          var arList = []
          var reg = new RegExp('</?em>', 'g')
          for (var index in res.result.articleList) {
            var article = changeToVo(res.result.articleList[index])
            article.title = article.title.replace(reg, '')
            arList.push(article)
          }
          commit(types.NEWS_SET_LIST_ARTICLE_2, arList.splice(1, 6))
        }
      })
    } else {
      post(Api.RECOMMEND_ARTICLE_BY_TAG, tags).then(function (res) {
        commit(types.NEWS_SET_LIST_ARTICLE_2, res)
      })
    }
  },
  [ types.NEWS_RECOMMEND_ARTICLE_BY_TEMP_TAG ]: ({commit, state}) => {
    post(Api.RECOMMEND_ARTICLE_BY_TAG, state.tempGraph).then(function (res) {
      commit(types.NEWS_SET_LIST_ARTICLE_3, res)
    })
  },
  [ types.NEWS_RECOMMEND_BY_ARTICLE ]: ({commit, state}, param) => {
    var url = Api.RECOMMEND_ARTICLE_BY_ARTICLE.replace('{uid}', param.uid).replace('{iid}', param.iid)
    get(url).then(function (res) {
      commit(types.NEWS_SET_LIST_ARTICLE_2, res)
    })
  },
  [ types.NEWS_SEARCH_WITH_KEY ]: ({commit, state}, param) => {
    var hm = 20
    var pageNum = 1
    if (param.pageNum) {
      pageNum = param.pageNum
    }
    if (param.hm) {
      hm = param.hm
    }
    var url = Api.GET_ARTICLE_WITH_KEY_SEARCH.replace('{site}', param.site).replace('{key}', param.key).replace('{hm}', hm).replace('{pageNum}', pageNum)
    get(url).then(function (res) {
      if (res.result.articleList) {
        var arList = []
        for (var index in res.result.articleList) {
          arList.push(changeToVo(res.result.articleList[index]))
        }
        if (param.loadMore) {
          commit(types.NEWS_ADD_LIST_ARTICLE_1, arList)
        } else {
          commit(types.NEWS_SET_LIST_ARTICLE_1, arList)
        }
      }
    })
  },
  [ types.NEWS_RECOMMEND_ARTICLE_BY_SITE ]: ({commit, state}, param) => {
    var hm = 50
    var pageNum = 1
    if (param.pageNum) {
      pageNum = param.pageNum
    }
    var url = Api.GET_ARTICLE_BY_SITE.replace('{site}', param.caId).replace('{pageNum}', pageNum).replace('{hm}', hm)
    get(url).then(function (res) {
      if (res.result.articleList) {
        var arList = []
        for (var index in res.result.articleList) {
          arList.push(changeToVo(res.result.articleList[index]))
        }
        const bhv = state.user.behavior
        for (var index1 in arList) {
          var caId = arList[index1].caId
          arList[index1].score = bhv[caId] ? bhv[caId] : 0
        }
        arList = arList.sort(function (a, b) {
          return b.score - a.score
        })
        var arSize = arList.length - 1
        if (arList[0].score > 0 && arList[arSize].score === 0) {
          for (; arSize > 0; arSize--) {
            if (arList[arSize].score === 0 && Math.random() > 0.5) {
              arList.pop()
            }
          }
        }
        if (param.loadMore) {
          commit(types.NEWS_ADD_LIST_ARTICLE_1, arList)
        } else {
          commit(types.NEWS_SET_LIST_ARTICLE_1, arList)
        }
      }
    })
  },
  [ types.NEWS_COLLECT_ARTICLE ]: ({commit, state}, param) => {
    //  COLLECT_ARTICLE: '/hdds/bvlg//bhv/clt/headlines/{uid}/{iid}/{type}'
    var url = Api.COLLECT_ARTICLE.replace('{uid}', state.user.id).replace('{iid}', param.iid).replace('{type}', param.type)
    return get(url).then(function (res) {
      return res
    })
  },
  [ types.NEWS_GET_ARTICLE_BY_XY_SITE ]: ({commit, state}, param) => {
    var url = Api.GET_ARTCILE_FROM_XY_SITE.replace('{site}', param.site).replace('{xy}', param.xy).replace('{range}', param.range)
    return get(url).then(function (res) {
      var arList = []
      for (var index in res.articleList) {
        arList.push(changeSimpleToVo(res.articleList[index]))
      }
      return arList
    })
  },
  [ types.NEWS_RECOMMEND_ARTICLE_BY_CATAGORY ]: ({commit, state}, param) => {
    var hm = 20
    if (param.hm) {
      hm = param.hm
    }
    var pageNum = 1
    if (param.pageNum) {
      pageNum = param.pageNum
    }
    var url = ''
    var threshold = 0.5
    if (param.key) {
      url = Api.GET_ARTICLE_BY_CATAGORY.replace('{site}', param.site).replace('{key}', param.key).replace('{pageNum}', pageNum).replace('{hm}', hm)
    } else {
      url = Api.GET_ARTICLE_BY_SITE.replace('{site}', param.site).replace('{pageNum}', pageNum).replace('{hm}', 50)
      threshold = 0.1
    }
    get(url).then(function (res) {
      if (res.result.articleList) {
        var arList = []
        for (var index in res.result.articleList) {
          arList.push(changeToVo(res.result.articleList[index]))
        }
        const bhv = state.user.behavior
        for (var index1 in arList) {
          var caId = arList[index1].caId
          arList[index1].score = bhv[caId] ? bhv[caId] : 0
        }
        arList = arList.sort(function (a, b) {
          return b.score - a.score
        })
        var arSize = arList.length - 1
        if (arList[0].score > 0 && arList[arSize].score === 0) {
          for (; arSize > 0; arSize--) {
            if (arList[arSize].score === 0 && Math.random() > threshold) {
              arList[arSize] = null
            }
          }
        }
        arList = arList.filter(ar => ar !== null)
        arSize = arList.length
        if (arSize === 0) {
          commit(types.NEWS_INIT_PAGE_SET, this.getCurrPageNum + 1)
          param.pageNum = param.pageNum + 1
          this.$store.dispatch(types.NEWS_RECOMMEND_ARTICLE_BY_CATAGORY, param)
        } else {
          if (param.loadMore) {
            commit(types.NEWS_ADD_LIST_ARTICLE_1, arList)
          } else {
            commit(types.NEWS_SET_LIST_ARTICLE_1, arList)
          }
        }
      }
    })
  }})

export { types }
export default module

export const personalConfig = {
  get () {
    return this.$store.state.news.personal
  },
  set (val) {
    this.$store.commit(types.NEWS_SET_PERSONAL, val)
  }
}

export const configureVisible = {
  get () {
    return this.$store.state.news.showConfigView
  },
  set (val) {
    this.$store.commit(types.NEWS_SET_SHOW_CONFIG_VIEW, val)
  }
}
