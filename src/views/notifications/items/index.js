export { default as FeedNotifyItem } from './FeedNotifyItem'
export { default as PostNotifyItem } from './PostNotifyItem'
export { default as GroupNotifyItem } from './GroupNotifyItem'
export { default as CommentNotifyItem } from './CommentNotifyItem'
export { default as UserNotifyItem } from './UserNotifyItem'
export { default as ActivityNotifyItem } from './ActivityNotifyItem'
export { default as ImageNotifyItem } from './ImageNotifyItem'
export { default as VideoNotifyItem } from './VideoNotifyItem'
export { default as DocumentNotifyItem } from './DocumentNotifyItem'

export default function getNotifyItemViewName (item) {
  if (item.operType.indexOf('feed:') === 0 || item.operType.indexOf('feedref:') === 0) {
    return 'feed-notify-item'
  } else if (item.operObjType === 'POST') {
    return 'post-notify-item'
  } else if (item.operObjType === 'GROUP') {
    return 'group-notify-item'
  } else if (item.operObjType === 'COMMENT') {
    return 'comment-notify-item'
  } else if (item.operObjType === 'USER') {
    return 'user-notify-item'
  } else if (item.operObjType === 'ACTIVITY') {
    return 'activity-notify-item'
  } else if (item.operObjType === 'IMAGE') {
    return 'image-notify-item'
  } else if (item.operObjType === 'VIDEO') {
    return 'video-notify-item'
  } else if (item.operObjType === 'DOCUMENT') {
    return 'document-notify-item'
  }
  return 'feed-notify-item'
}
