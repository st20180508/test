  function Fullscreen (element) {
    this.el = element
    return this
  }

  Fullscreen.prototype.init = function () {
    if (!document.fullscreenEnabled && !document.webkitFullscreenEnabled && !document.mozFullScreenEnabled && !document.msFullscreenEnabled) {
      return
    } else {
      this.fullScreen()
    }
  }

  Fullscreen.prototype.requestFullscreen = function () {
    var el = this.el
    if (el.requestFullscreen) {
      el.requestFullscreen()
    } else if (el.msRequestFullscreen) {
      el.msRequestFullscreen()
    } else if (el.mozRequestFullScreen) {
      el.mozRequestFullScreen()
    } else if (el.webkitRequestFullscreen) {
      el.webkitRequestFullscreen()
    }
  }

  Fullscreen.prototype.exitFullscreen = function () {
    if (document.exitFullscreen) {
      document.exitFullscreen()
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen()
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen()
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen()
    }
  }

  Fullscreen.prototype.fullScreen = function () {
    var _this = this
    if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
      _this.requestFullscreen()
    } else {
      _this.exitFullscreen()
    }
  }

  Fullscreen.prototype.destroy = function () {
    // exit from fullscreen if activated
    this.exitFullscreen()
  }
  export function fullscreen (element) {
    return new Fullscreen(element)
  }
