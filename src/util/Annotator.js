import { throttle, wrapUrl, isElementInContainerView } from '@/util'

const annotatorLocale = require('json-loader!po-loader!annotator2/locale/zh_CN/annotator.po')
Object.values(annotatorLocale).forEach(v => {
  if (Object.prototype.toString.call(v) === '[object Array]' && v[0] === null) {
    v.splice(0, 1)
  }
})
class AnnotatorGettext {
  constructor (options) {
    if (JSON.stringify(options) !== '{"domain":"annotator"}') {
      throw new Error('not support')
    }
  }
  gettext (msgid) {
    return annotatorLocale[msgid] || msgid
  }
}

annotatorLocale.Comments = ['写些什么...']

window.AnnotatorGettext = AnnotatorGettext

const annotator = require('annotator2')

// HTML template for this.element.
annotator.ui.editor.Editor.template = [
  '<div class="annotator-outer annotator-editor annotator-hide annotators">',
  '  <form class="annotator-widget">',
  '    <ul class="annotator-listing"></ul>',
  '    <div class="annotator-controls">',
  '     <button class="annotator-cancel el-button el-button--default">取消</button>',
  '     <button class="annotator-save annotator-focus el-button el-button--primary">保存</button>',
  '    </div>',
  '  </form>',
  '</div>'
].join('\n')

annotator.ui.viewer.Viewer.options.confirmBeforeDelete = false
annotator.ui.viewer.Viewer.template = [
  '<div class="annotator-outer annotator-viewer annotator-hide">',
  '  <ul class="annotator-widget annotator-listing"></ul>',
  '</div>'
].join('\n')

annotator.ui.textselector.TextSelector.options.selectOnBody = false

class Annotator {
  constructor (options) {
    this.options = options
    if (this.isDoc() && options.pdfViewer && !options.elem && !options.container) {
      this.container = options.pdfViewer.$el.querySelector('.viewerContainer')
      this.elem = options.pdfViewer.$refs.viewer
    } else {
      this.container = options.container
      this.elem = options.elem || window.document.body
    }

    this.app = null
    this.anns = []
    this.delAnns = []
    this.bucketBarElems = null
    this.bucketBarTop = 0
    this.bucketBarBottom = 0
    this.locateAnn = null
    this.highlighter = null
    this.selectionAnn = null
    this.shareElem = null
    this.shareFunc = null
    this.collectElem = null
    this.collectFunc = null
    this.disable = !!options.disable
  }

  start () {
    this.highlighter = new annotator.ui.highlighter.Highlighter(this.elem)
    let customModule = () => ({
      annotationsLoaded: (anns, fromMe) => {
        if (!fromMe) {
          this.annsPush(...anns)
          if (this.options.loadedCallback) {
            this.options.loadedCallback()
          }
        }
      },
      beforeAnnotationCreated: ann => {
        ann.objId = this.options.objId
        ann.objType = this.options.objType
        ann.userId = this.options.userId
      },
      annotationCreated: ann => {
        this.annsPush(ann)
        this.updateBucketBar()
        if (this.options.createdCallback) {
          this.options.createdCallback(this.clearCopyAnn(ann))
        }
      },
      annotationUpdated: ann => {
        if (this.options.updatedCallback) {
          this.options.updatedCallback(this.clearCopyAnn(ann))
        }
      },
      beforeAnnotationDeleted: ann => {
        this.delAnns.push(JSON.parse(JSON.stringify(ann)))
      },
      annotationDeleted: ann => {
        this.anns = this.anns.filter(a => a.id !== ann.id)
        this.applyAnns()
        let delId = ann.id + '' // 被删除的id为数字类型，原来的为字符串类型，需要进行转换
        let delAnn = this.delAnns.filter(a => a.id === delId)
        this.delAnns = this.delAnns.filter(a => a.id !== delId)
        this.updateBucketBar()
        if (this.options.deletedCallback) {
          this.options.deletedCallback(this.clearCopyAnn(delAnn[0]))
        }
      }
    })
    let adderHandleExtension = a => {
      this.adder = a
    }
    let editorHandleExtension = a => {
      this.editor = a
    }
    let viewerHandleExtension = a => {
      this.viewer = a
      if (this.options.viewerCustomElem) {
        this.viewer.addField({
          load: (field, ann) => {
            this.options.viewerCustomElem(field, ann)
          }
        })
      }
    }
    let app = this.app = new annotator.App()
      .include(annotator.ui.main, {
        element: this.elem,
        adderExtensions: [adderHandleExtension],
        editorExtensions: [editorHandleExtension],
        viewerExtensions: [viewerHandleExtension],
        adderOptions: {
          beforeCreate: ann => {
            if (ann) { // 默认仅当前用户可编辑，其他用户可查看
              if (!ann.permissions) {
                ann.permissions = {}
              }
              ann.permissions.admin = []
              ann.permissions.update = [this.options.userId]
              ann.permissions['delete'] = [this.options.userId]
            }
          }
        },
        textselectorOptions: {
          isDisable: () => this.disable
        }
      })
      .include(annotator.storage.http, {
        prefix: wrapUrl('/page/annotator'),
        emulateJSON: 'true',
        onError: (msg, xhr) => {
          if (this.options.messageCallback) {
            this.options.messageCallback('error', msg, xhr)
          } else {
            console.error(msg, xhr)
          }
        }
      })
      .include(customModule)
      // .include(annotator.ui.filter.standalone)
    app.registry.registerUtility((message, severity) => {
      if (this.options.messageCallback) {
        this.options.messageCallback(severity, message)
      } else {
        console.error(severity, message)
      }
    }, 'notifier')
    if (this.options.adderShare || this.options.adderCollect) {
      annotator.ui.adder.Adder.template = `
        <div class="annotator-adder annotator-hide el-button-group">
          <button title="${annotator.util.gettext('Annotate')}" type="button" class="el-button"><i class="el-icon-edit"></i></button>`
      if (this.options.adderCollect) {
        annotator.ui.adder.Adder.template = annotator.ui.adder.Adder.template + `<button title="收藏" class="annotator-adder-collect el-button"><i class="el-icon-star-on"></i></button>`
      }
      if (this.options.adderShare) {
        annotator.ui.adder.Adder.template = annotator.ui.adder.Adder.template + `<button title="分享" class="annotator-adder-share el-button"><i class="el-icon-share"></i></button>`
      }
      annotator.ui.adder.Adder.template = annotator.ui.adder.Adder.template + `</div>`
    }
    //   apply: (target, ctx, args) => {
    //     this.selectionAnn = args[0]
    //     return Reflect.apply(target, ctx, args)
    //   }
    // })

    return app.start().then(() => {
      if (this.options.adderShare) {
        this.shareElem = window.document.querySelector('.annotator-adder .annotator-adder-share')
        if (this.shareElem) {
          this.shareFunc = e => {
            this.options.adderShare(this.getSelectionAnn())
            this.adder.hide()
            this.adder.ignoreMouseup = false
            e.stopPropagation()
          }
          this.shareElem.addEventListener('click', this.shareFunc, false)
        }
      }
      if (this.options.adderCollect) {
        this.collectElem = window.document.querySelector('.annotator-adder .annotator-adder-collect')
        if (this.collectElem) {
          this.collectFunc = e => {
            this.options.adderCollect(this.getSelectionAnn())
            this.adder.hide()
            this.adder.ignoreMouseup = false
            e.stopPropagation()
          }
          this.collectElem.addEventListener('click', this.collectFunc, false)
        }
      }
      app.ident.identity = this.options.userId
      // app.annotations.create = new Proxy(app.annotations.create, {
      //   apply: (target, ctx, args) => {
      //     args.forEach(ann => {
      //       if (ann) { // 默认仅当前用户可编辑，其他用户可查看
      //         if (!ann.permissions) {
      //           ann.permissions = {}
      //         }
      //         ann.permissions.admin = []
      //         ann.permissions.update = [this.options.userId]
      //         ann.permissions['delete'] = [this.options.userId]
      //       }
      //     })
      //     return Reflect.apply(target, ctx, args)
      //   }
      // })

      return app.annotations.load({
        objId: this.options.objId,
        objType: this.options.objType
      }).then(() => {
        if (this.elem && this.options.bucketBar) {
          let bcr = this.getContainerRect()
          this.bucketBarTop = bcr.top + 8
          this.bucketBarBottom = bcr.bottom - 28
          let root = window.document.createElement('div')
          root.className = 'bucket_bar'
          this.bucketBarElems = {
            root,
            upper: root.querySelector('.upper_indicator'),
            lower: root.querySelector('.lower_indicator'),
            indicator: root.querySelector('.indicators')
          }
          root.onclick = (e) => {
            let el = e.target
            let cls = el.className
            if (cls) {
              let size = parseInt(el.innerText || 0)
              if (size > 0) {
                let ix = -1
                if (cls.indexOf('upper_indicator') >= 0) {
                  ix = size - 1
                } else if (cls.indexOf('lower_indicator') >= 0) {
                  ix = this.anns.length - size
                }
                if (ix >= 0) {
                  this.goToAnnIndex(ix)
                }
              }
            }
          }
          let el = this.elem.parentElement
          if (el.tagName === 'HTML') {
            el = this.elem
          }
          el.appendChild(root)
          this.bucketBarChange = throttle(e => {
            if (e.type === 'resize') {
              let bcr = this.getContainerRect()
              this.bucketBarTop = bcr.top + 8
              this.bucketBarBottom = bcr.bottom - 28
            }
            this.updateBucketBar()
          }, 100)
          if (!this.isDoc()) {
            this.updateBucketBar()
          }
          window.addEventListener('scroll', this.bucketBarChange, true)
          window.addEventListener('resize', this.bucketBarChange, true)
        }
      }).catch(e => {
        console.error('annotator start fail', e)
      })
    })
  }

  highlight () {
    let uanns = this.anns.filter(({id, _local}) => {
      let pass = !_local || !_local.highlights
      if (!pass && _local.highlights) {
        if (_local.highlights.length === 0) {
          pass = true
        } else if (!this.elem.querySelector(`span[data-annotation-id="${id}"]`)) {
          pass = true
          _local.highlights = []
        }
      }
      return pass
    })
    if (uanns.length > 0) {
      this.app.runHook('annotationsLoaded', [uanns, true])
      .then(() => {
        this.updateBucketBar()
      })
      if (this.locateAnn) { // 延迟定位
        let ann = this.locateAnn
        this.locateAnn = null
        this.goTohighlight(ann)
      }
    }
  }

  updateBucketBar () {
    if (!this.anns || this.anns.length === 0) {
      this.bucketBarElems.root.innerHTML = ''
    } else {
      let crect = {
        clientRect: this.getContainerRect(),
        clientHeight: this.container ? this.container.clientHeight : window.innerHeight,
        clientWidth: this.container ? this.container.clientWidth : window.innerWidth
      }
      let annEls = this.anns.map(({_local, ranges, elIndex}) => {
        let el, rect
        if (_local && _local.highlights && _local.highlights.length > 0) {
          el = _local.highlights[0]
          let r = el.getBoundingClientRect()
          rect = {
            top: r.top,
            left: r.left,
            bottom: r.bottom,
            right: r.right,
            width: r.width,
            height: r.height
          }
        }
        return {
          el,
          rect,
          elIndex
        }
      })
      let pos = {
        upper: 0,
        start: -1,
        end: -1,
        lower: 0
      }
      annEls.forEach(({el, rect}, i) => {
        if (el && isElementInContainerView(null, crect, false, rect)) {
          if (pos.start < 0) {
            pos.start = i
          }
          pos.end = i
        }
      })
      if (pos.start >= 0) {
        pos.upper = pos.start
      }
      pos.lower = this.anns.length - 1 - pos.end
      if (pos.start < 0 && pos.end < 0) { // 可视范围内无标记，需要获取可视区域上下部分的标记条数
        pos.upper = 0
        pos.lower = 0
        let start = -1
        let end = -1
        if (this.isDoc()) {
          let pages = [...this.elem.children].filter(el => isElementInContainerView(el, crect, true))
          .map(el => parseInt(el.getAttribute('data-page-number') || 0))
          let startPage = pages[0]
          let endPage = pages.length > 1 ? pages[pages.length - 1] : startPage
          annEls.forEach(({el, elIndex}, i) => {
            if (elIndex <= startPage) {
              start = i
            }
            if (elIndex >= endPage) {
              if (end < 0) {
                end = i
              }
            }
          })
        } else {
          annEls.forEach(({rect}, i) => {
            if (rect.top <= rect.height) {
              start = i
            }
            if (rect.top >= rect.height) {
              if (end < 0) {
                end = i
              }
            }
          })
        }
        if (start >= 0 && end >= 0) {
          if (start === end) {
            if (start === 0) {
              pos.upper = 0
            } else if (start + 1 === annEls.length) {
              pos.upper = annEls.length
            } else {
              pos.upper = start
            }
          } else {
            pos.upper = start + 1
          }
          pos.lower = annEls.length - pos.upper
        } else if (start >= 0 && end < 0) {
          pos.upper = annEls.length
        } else if (start < 0 && end >= 0) {
          pos.lower = annEls.length
        }
      }
      let htm = []
      if (pos.upper > 0) {
        htm.push(`<div style="position: fixed; top: ${this.bucketBarTop}px;" class="upper_indicator">${pos.upper}</div>`)
      }
      if (pos.start >= 0 && pos.end >= 0) {
        let scount = 0
        for (let i = pos.start; i <= pos.end; i++) {
          let rect = annEls[i].rect
          let next = i < pos.end ? annEls[i + 1].rect : null
          if (next && next.top === rect.top) {
            scount++
          } else {
            htm.push(`<div class="indicators" style="position: fixed; top: ${rect.top}px;">${scount + 1}</div>`)
            scount = 0
          }
        }
      }
      if (pos.lower > 0) {
        htm.push(`<div style="position: fixed; top: ${this.bucketBarBottom}px;" class="lower_indicator">${pos.lower}</div>`)
      }
      this.bucketBarElems.root.innerHTML = htm.join('')
    }
  }

  clearAnn (ann) {
    if (ann) {
      delete ann._local
      delete ann.elIndex
      delete ann.sortr0
    }
    return ann
  }

  clearCopyAnn (ann) {
    if (ann) {
      return this.clearAnn({...ann})
    }
    return ann
  }

  highlightAnn (ann) {
    let an = this.anns.filter(a => a.id === ann.id)[0]
    if (an) {
      this.clearAnn(ann)
      Object.assign(an, ann)
    } else {
      this.annsPush(ann)
    }
    this.highlight()
  }

  unHighlightAnnById (id) {
    this.unHighlightAnn({id})
  }

  unHighlightAnn (ann) {
    let an = this.anns.filter(a => a.id === ann.id)[0]
    if (an) {
      this.highlighter.undraw(an)
      // 代替移除功能，远程删除了本地隐藏处理
      this.anns = this.anns.filter(a => a.id !== an.id)
      this.applyAnns()
      this.updateBucketBar()
    }
  }

  goToAnn (id) {
    let anns = this.anns.filter(a => a.id === id)
    this.goTohighlight(anns[0])
  }

  goToAnnIndex (index) {
    let ann = this.anns[index]
    this.goTohighlight(ann)
  }

  goTohighlight (ann) {
    if (ann) {
      let el = this.elem.querySelector(`span[data-annotation-id="${ann.id}"]`)
      if (el) {
        el.scrollIntoView()
        if (this.container) {
          this.container.scroll(0, this.container.scrollTop - this.container.clientHeight / 2) // 居中显示
        } else {
          window.scroll(0, window.pageYOffset - window.innerHeight / 2)
        }
      } else {
        if (this.isDoc()) {
          let page = ann.elIndex
          let pel = this.elem.querySelector(`div[data-page-number="${page}"]`)
          if (pel) {
            pel.scrollIntoView() // 页面未加载，通过滚动触发页面加载
            this.locateAnn = ann // 延迟定位，在页面加载后
          }
        }
      }
    }
  }

  annsPush (...ans) {
    this.anns.push(...ans)
    this.applyAnns()
  }

  getSelectionAnn () {
    return this.adder ? this.adder.annotation : null
  }

  setDisable (bool) {
    this.disable = bool
  }

  applyAnns () {
    if (this.isDoc()) {
      this.anns.forEach(ann => {
        let range = ann.ranges[0]
        if (range) {
          let p = /\/\w+\[(\d+)\].*/
          if (p.test(range.start)) {
            ann.elIndex = parseInt(range.start.replace(/\/\w+\[(\d+)\].*/, '$1'))
            ann.sortr0 = range.start.replace(/\/\w+\[(\d+)\]/g, '$1,')
            .split(',')
            .filter(o => !!o)
            .map(o => parseInt(o))
          }
        }
      })
    } else {
      let allLoaded = this.anns.every(ann => ann._local && ann._local.highlights[0])
      if (allLoaded) {
        this.anns.forEach(ann => {
          let el = ann._local.highlights[0]
          let bcr = el.getBoundingClientRect()
          ann.sortr0 = [ bcr.top ]
        })
      } else { // 不支持，只加载部分批注方式
        console.error('BucketBar not support')
        return
      }
    }
    // 从小到大排序定位信息
    this.anns.sort(({sortr0: a}, {sortr0: b}) => a.reduce((v, x, i) => v === 0 ? (i < b.length ? x - b[i] : -1) : v, 0))
  }

  isDoc () {
    return this.options.objType === 'doc'
  }

  getContainerRect () {
    return this.container ? this.container.getBoundingClientRect() : {
      top: 0,
      left: 0,
      bottom: window.innerHeight,
      right: window.innerWidth,
      height: window.innerHeight,
      width: window.innerWidth
    }
  }

  destroy () {
    if (this.app) {
      this.app.destroy()
    }
    if (this.bucketBarChange) {
      window.removeEventListener('scroll', this.bucketBarChange, true)
      window.removeEventListener('resize', this.bucketBarChange, true)
      this.bucketBarChange = null
      if (this.bucketBarElems && this.bucketBarElems.root && this.bucketBarElems.root.parentElement) {
        this.bucketBarElems.root.parentElement.removeChild(this.bucketBarElems.root)
      }
    }
    this.app = null
    this.elem = null
    this.container = null
    this.anns = null
    this.bucketBarElems = null
    this.locateAnn = null
    this.highlighter = null
    this.selectionAnn = null
    if (this.shareElem) {
      this.shareElem.removeEventListener('click', this.shareFunc, false)
    }
    if (this.collectElem) {
      this.collectElem.removeEventListener('click', this.collectFunc, false)
    }
    this.shareElem = null
    this.shareFunc = null
    this.collectElem = null
    this.collectFunc = null
  }

}

export default Annotator
