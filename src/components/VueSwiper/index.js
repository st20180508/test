/**
 * Vue-awesome-swiper
 * @author Surmon.me
 */

// var Swiper = require('../../../slib/swiper.js')
// var SwiperComponent = require('./Swiper.vue')
const SwiperComponent = () => import(/* webpackChunkName: "swiper" */ './Swiper.vue')
var SlideComponent = require('./slide.vue')
// SwiperComponent = SwiperComponent.default || SwiperComponent
// SlideComponent = SlideComponent.default || SlideComponent
// if (typeof window !== 'undefined') {
//   window.Swiper = Swiper
// }

var swiper = {
  // swiperSlide: SlideComponent,
  // swiper: SwiperComponent,
  // swiperPlugins: Swiper.prototype.plugins,
  install: function (Vue) {
    Vue.component('swiper', SwiperComponent)
    Vue.component('swiper-slide', SlideComponent)
  }
}

module.exports = swiper
