export { default as ShortTextItem } from './ShortTextItem'
export { default as LongTextItem } from './LongTextItem'
export { default as ImageItem } from './ImageItem'
export { default as DocumentItem } from './DocumentItem'
export { default as VideoItem } from './VideoItem'
export { default as MusicItem } from './MusicItem'
export { default as AudioItem } from './AudioItem'
export { default as PostItem } from './PostItem'
export { default as ShareItem } from './ShareItem'
export { default as ActivityItem } from './ActivityItem'
export { default as ReprintItem } from './ReprintItem'

export default function getFeedItemViewName (item) {
  if (item.objType === 'POST') {
    if (item.objContent) {
      let modelObject = item.objContent.modelObject ? item.objContent.modelObject : item.objContent
      if (modelObject.contentType === 'normal') { // 普通帖子类型，包含：文本、图片、视频
        let postType = modelObject.postType
        if (postType === 'image') {
          return 'image-item'
        } else if (postType === 'video') {
          return 'video-item'
        } else if (postType === 'doc') {
          return 'document-item'
        }
      } else if (modelObject.contentType === 'share') {
        return 'share-item'
      } else if (modelObject.contentType === 'activity') {
        return 'activity-item'
      } else if (modelObject.contentType === 'blog') {
        return 'long-text-item'
      } else if (modelObject.contentType === 'reprint') {
        return 'reprint-item'
      }
    }
    return 'post-item'
  }
  return 'short-text-item'
}
