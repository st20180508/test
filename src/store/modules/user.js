import { buildModule } from '@/store/module'
import { rest } from '@/util'

const { types, module } = buildModule({
  name: 'user',
  restUrl: '/upms/users',
  restListQuery: 'deptRole={{deptId}}&sort=id,asc',
  types: {
    A_GET_ROLES: 'A_GET_ROLES'
  },
  state: {
    roles: [],
    commentUsers: {}
  }
})

Object.assign(module.getters, {
})

Object.assign(module.mutations, {
})

Object.assign(module.actions, {
  [ types.A_GET_ROLES ] ({ dispatch, commit, state }, userId) {
    return rest.get('/upms/users/' + userId + '/roles')
  }
})

export { types }
export default module
