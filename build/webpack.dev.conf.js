var utils = require('./utils')
var webpack = require('webpack')
var config = require('../config')
var merge = require('webpack-merge')
var baseWebpackConfig = require('./webpack.base.conf')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')

var pages = ['app']
if (config.multiPageRoot) {
  var entries = utils.getEntries(config.multiPageRoot)
  pages.push(...entries.map(en => en.name))
  baseWebpackConfig.plugins = []
  entries.forEach(function (en) {
    var name = en.name
    baseWebpackConfig.entry[name] = en.path
    baseWebpackConfig.plugins.push(new HtmlWebpackPlugin({
      filename: en.filename,
      template: en.template,
      excludeChunks: pages.filter(p => p !== name),
      inject: true
    }))
  })
}

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
  baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name])
})

module.exports = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({ sourceMap: config.dev.cssSourceMap })
  },
  // cheap-module-eval-source-map is faster for development
  devtool: '#cheap-module-eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    // https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      excludeChunks: pages.filter(p => p !== 'app'),
      inject: true
    }),
    new FriendlyErrorsPlugin()
  ]
})
