import { rest } from '@/util'
import { types as dataPoolTypes } from '@/store/modules/dataPool'

export default {
  data () {
    return {
      isFollowRequest: false,
      followRequestCount: 0
    }
  },
  computed: {
    currUser () {
      return this.$store.state.currUser || {}
    }
  },
  watch: {
  },
  created () {
  },
  methods: {
    getUserSex (user) {
//      1：男 2：女 0：保密
      return user && user.sex ? user.sex : 0
    },
    getUserAvatar (user) {
      return user && user.avatar ? user.avatar : ''
    },
    getUserOrgAvatar (user) {
      return user && user.extInfoObject && user.extInfoObject.org_avatar ? user.extInfoObject.org_avatar : this.getUserAvatar(user)
    },
    getUserDept (user) {
      return user && user.deptRole ? user.deptRole : {}
    },
    getJoinTime (user) {
      return user && user.createTime ? new Date(user.createTime) : ''
    },
    getFollowCount (user) {
      if (user && user.snsInfo && user.snsInfo.followCount > 0) {
        return user.snsInfo.followCount
      } else {
        return 0
      }
    },
    getFansCount (user) {
      if (user && user.snsInfo && user.snsInfo.fansCount > 0) {
        return user.snsInfo.fansCount
      } else {
        return 0
      }
    },
    getFavoriteCount (user) {
      if (user && user.snsInfo && user.snsInfo.favoriteCount > 0) {
        return user.snsInfo.favoriteCount
      } else {
        return 0
      }
    },
    getUserIntro (user) {
      let intro = this.getUserExtInfoObject(user).intro
      if (!intro) {
        intro = '这个人太懒了，什么都没有留下~'
      }
      return intro
    },
    getUserExtInfoObject (user) {
      let userExtInfoObject
      if (user && user.extInfoObject) {
        userExtInfoObject = user.extInfoObject
      } else {
        userExtInfoObject = {}
      }
      return userExtInfoObject
    },
    isFollow (user) {
      if (user && user.relation) {
        return user.relation.isCurrUserFollow
      } else {
        return false
      }
    },
    follow (user) {
      if (!user || !user.id) {
        return
      }
      if (this.isFollowRequest) {
        return
      }
      if (this.followRequestCount > 5) {
        return
      }
      this.isFollowRequest = true
      let follow = !this.isFollow(user)
      rest.create('/sns/followers/user?userId=' + user.id + '&follow=' + follow, {}).then((isSuccess) => {
        if (isSuccess) {
          // 更新用户粉丝数量，关注状态
          let userCopy = JSON.parse(JSON.stringify(user))
          userCopy.relation = userCopy.relation ? userCopy.relation : {}
          userCopy.snsInfo = userCopy.snsInfo ? userCopy.snsInfo : {}
          userCopy.relation.isCurrUserFollow = follow
          userCopy.snsInfo.fansCount = userCopy.snsInfo.fansCount ? userCopy.snsInfo.fansCount : 0
          if (follow) {
            userCopy.snsInfo.fansCount ++
          } else {
            userCopy.snsInfo.fansCount --
            if (userCopy.snsInfo.fansCount < 0) {
              userCopy.snsInfo.fansCount = 0
            }
          }
          this.$store.commit(dataPoolTypes.M_ADD_USERS, [userCopy])
        } else {
          this.msgError('关注失败！')
        }
        this.isFollowRequest = false
        this.followRequestCount ++
      }).catch((e) => {
        console.error(follow ? '关注失败！' : '取消关注失败！', e)
        this.msgError(follow ? '关注失败，请稍候重试！' : '取消关注失败，请稍候重试！')
        this.isFollowRequest = false
        this.followRequestCount ++
      })
    },
    matchCurrUser (user) {
      if (user && this.currUser) {
        return user.id === this.currUser.id
      }
      return false
    },
    loadUserOtherInfo (user) {
      if (!user || !user.id) {
        return
      }
      let userIds = []
      if (Array.isArray(user)) {
        user.forEach(u => {
          if (u && !u.isLoadedOtherInfo) {
            userIds.push(u.id)
          }
        })
      } else if (user && !user.isLoadedOtherInfo) {
        userIds.push(user.id)
      }
      // 判断加载用户扩展信息
      if (userIds && userIds.length) { // user.isFollow === null ||
        this.isLoading = true
        rest.get('/sns/user/otherInfo', {userId: userIds, loadRelation: true}, true).then((otherInfo) => {
          if (otherInfo) {
            userIds.forEach(id => {
              let userCopy = {id}
              userCopy.snsInfo = otherInfo.snsInfos[user.id] || {}
              userCopy.relation = otherInfo.relations[user.id] || {}
              userCopy.isLoadedOtherInfo = true
              this.$store.commit(dataPoolTypes.M_ADD_USERS, [userCopy])
            })
            this.isLoading = false
            this.loadComplete = true
          }
        }).catch((e) => {
          this.isLoading = false
          this.loadComplete = true
          console.error('加载用户其它信息异常！', e)
        })
      }
    }
  }
}
