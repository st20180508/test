export default () => {
  return Promise.all([
    import(/* webpackChunkName: "croppa" */ 'vue-croppa'),
    import(/* webpackChunkName: "croppa" */ 'vue-croppa/dist/vue-croppa.css')
  ])
  .then(([m, c]) => m.component)
}
