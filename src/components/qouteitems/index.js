export { default as QuoteText } from './QuoteText'
export { default as QuoteImage } from './QuoteImage'
export { default as QuoteVideo } from './QuoteVideo'

export default function getQuoteItemViewName (item) {
  if (!item || !item.commentBody) {
    return ''
  }
  let qouteTypes = {
    1: 'quote-image',
    2: 'quote-video',
    3: 'quote-text',
    6: 'quote-text',
    13: 'quote-text'
  }
  let view
  if (item.commentBody.commentQuote) {
    view = qouteTypes[item.commentBody.commentQuote.quoteObjType]
  }
  if (!view && item.ext) {
    view = 'quote-image'
  }
  return !view ? '' : view
}
