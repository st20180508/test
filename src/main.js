import 'es6-promise/auto'

import '@/assets/element/theme/index.css'
import Element from 'element-ui'
import '../static/css/font-awesome.min.css'

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import * as directives from './directives'
import * as filters from './filters'
import Components from './components'
import VueDND from 'awe-dnd'
import VueAwesomeSwiper from '@/components/VueSwiper'
import Util from '@/util'
import ModuleHelper from '@/store/modules/helper'
import BusPlugin from '@/util/busPlugin'

// 添加状态管理工具类
Vue.use(ModuleHelper)

// 添加工具类
Vue.use(Util)

// 添加全局Bus插件
Vue.use(BusPlugin)

// 拖拽插件
Vue.use(VueDND)

// 引入自定义组件
Vue.use(Components)

// 引入swiper组件
Vue.use(VueAwesomeSwiper)

// 引入“饿了么”界面组件
Vue.use(Element)

// sync the router with the vuex store
// this registers `store.state.route`
sync(store, router)

// register global utility directives.
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key])
})

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
