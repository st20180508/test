import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import * as getters from './getters'
import mutations from './mutations'
import storeModules from './modules'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

let modules = {}
Object.values(storeModules).forEach(m => {
  modules[m.name] = m
  delete m.name
})

const store = new Vuex.Store({
  actions,
  getters,
  mutations,
  modules,
  strict: debug,
  state: {
    documentHidden: false,
    currMediaQuery: '',
    currUser: {},
    showSidebar: true,
    showSidebarInSmall: false,
    currSite: {}
  }
})

export default store
