// import $ from 'jquery'
import axios from 'axios'
import qs from 'qs'

// axios.defaults.timeout = 30000;

export const inDev = process.env.NODE_ENV === 'development'
const useOauthInDev = process.env.OAUTH === 'true'

export function wrapUrl (url) {
  if (inDev) {
    let notPrefixCutInDev = process.env.PREFIX_CUT === 'false'
    if (useOauthInDev || notPrefixCutInDev) {
      return '/dev' + url
    }
    // 移除开发环境下的api前缀如：/page/feeds/group -> /dev/feeds/group
    let ix = url.indexOf('/', 1)
    ix = ix > 0 ? ix : 0
    return '/dev' + url.substring(ix)
  }
  return url
}

// 基础路径用于项目是非根路径下
export const basePath = process.env.BASE_PATH

export function annotatorPromise () {
  return import(/* webpackChunkName: "annotator" */ '@/util/Annotator')
  .then(m => m.default)
}

export function errorGoLogin (e) {
  if (inDev && useOauthInDev && e.status === 500 && e.message &&
      e.message.indexOf('Can\'t found access_token[') === 0) {
    window.document.location.href = '/logout?redirect=' + encodeURIComponent(window.document.location.href)
  }
  if (e.status === 401) {
    if (e.response) {
      let url = e.response.headers['loginurl']
      if (url) {
        window.document.location.href = url
      }
    }
  }
}

// 桌面通知，参考 https://developer.mozilla.org/zh-CN/docs/Web/API/notification/Using_Web_Notifications
export function getNotificationPermission (cb) {
  if (window.Notification && window.Notification.permission !== 'granted') {
    window.Notification.requestPermission(function (status) {
      if (window.Notification.permission !== status) {
        window.Notification.permission = status
      }
      if (cb && typeof cb === 'function') {
        cb(status)
      }
    })
  }
}

export const messagePrompt = {
  msgError (msg) {
    this.$message({
      showClose: true,
      message: msg,
      type: 'error',
      duration: 10000
    })
  },
  msgWarning (msg) {
    this.$message({
      message: msg,
      type: 'warning',
      duration: 5000
    })
  },
  msgSuccess (msg) {
    this.$message({
      message: msg,
      type: 'success'
    })
  },
  msgInfo (msg) {
    this.$message({
      message: msg,
      type: 'info'
    })
  }
}

export function camelToKebab (str) {
  return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
}

export function kebabToCamel (str) {
  return str.replace(/-([a-z])/g, g => g[1].toUpperCase())
}

export function dateFormat (date, format) {
  if (!date) {
    return date
  }
  let n = new Date(date)
  var d = Date.UTC(n.getFullYear(), n.getMonth(), n.getDate(), n.getHours(), n.getMinutes(), n.getSeconds(), n.getMilliseconds())
  return new Date(d).toISOString().slice(0, 19).replace('T', ' ')
}

export function dateYMD (date) {
  if (!date) {
    return date
  }
  let d = new Date(date)
  return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
}

export function convertAtUsers (content) {
  if (!content) {
    return Promise.resolve(content)
  }
  let userIds = []
  content.replace(/<a.*?\s+data-item-key="(\w+)".*?>.*?<\/a>/g, ($0, key) => {
    userIds.push(key)
    return ''
  })
  return this.storeNeedData([{
    type: 'user',
    dataIds: userIds
  }])
    .then(([users]) => {
      if (userIds.length === 0) {
        return content
      }
      return content.replace(/<a.*?\s+data-item-key="(\w+)".*?>.*?<\/a>/g, ($0, key) => {
        let name = users[key] ? users[key].userName : key
        return $0.replace(/(data-datas="@).*?"/, '$1' + name + '"').replace(/(href=").*?"/, '')
      })
    })
}

export function convertEmoji (content) {
  if (!content) {
    return ''
  }
  return content.replace(/<img.*?\s+data-face="(.*?)".*?>/g, ($0, $1) => {
    if ($1) {
      return $0.replace(/\s*src=".*?"/g, '').replace(/data-face="(.*?)"/, ($0, $1) => `${$0} src="${basePath}static/emoji/${$1}"`)
    } else {
      return $0
    }
  })
}

/**
 * 替换多行连续空的<p>标签
 * @param content html文本内容
 * @param count 连续几个以上，默认是1个以上
 * @param ext 替换的字符
 * @returns {*}
 */
export function cleanMorePLine (content, count, ext) {
  if (!content || content.trim().length <= 0) {
    return ''
  }
  let reg = /(<p><br \/><\/p>[\r\n|\n])+/g
  if (count > 1) {
    reg = new RegExp('(<p><br \\/><\\/p>[\\r\\n|\\n]){' + count + ',}', 'g')
  }
  let defExt = ''
  if (ext !== undefined && ext !== null && ext.length > 0) {
    defExt = ext
  }
  return content.replace(reg, defExt)
}

export function cleanHtml (content, len, ext, count) {
  if (!content) {
    return ''
  }
  if (!ext) {
    ext = ''
  }
  let pe = document.createElement('p')
  pe.innerHTML = content.replace(/<(?!(img|a|\/img|\/a)).*?>/g, '')
  let html = ''
  if (!count) {
    count = 0
  }
  let isOutLen = false
  for (let node of pe.childNodes) {
    if (node.nodeName === 'IMG') {
      if (node.dataset.face) {
        count = count + 3
        if (count <= len) {
          html = html + node.outerHTML
        } else {
          isOutLen = true
        }
      }
    } else if (node.nodeName === 'A') {
      if (node.dataset.atwhoAtQuery) {
        count = count + node.dataset.datas.length
        if (count <= len) {
          html = html + node.outerHTML
        } else {
          isOutLen = true
        }
      } else { // a 标签可能嵌套img标签或者文本内容
        let innerContent = cleanHtml(node.innerHTML, len, '<!-$END#->', count)
        if (innerContent.indexOf('<!-$END#->') >= 0) {
          isOutLen = true
          html = html + innerContent.substring(0, innerContent.length - 10)
        } else {
          html = html + innerContent
        }
      }
    } else if (node.nodeName === '#text') {
      let value = node.nodeValue
      if (count + value.length > len) {
        isOutLen = true
        html = html + value.substring(0, len - count)
        count = len
      } else {
        html = html + value
        count = count + value.length
      }
    }
  }
  html = convertEmoji(html)
  html = html.replace(/\r/g, '').replace(/\n/g, ' ').replace(/\s+/g, ' ')
  return isOutLen ? html + ext : html
}

export function extractImage (htmlContent) {
  let images = []
  let contentBody = document.createElement('div')
  contentBody.innerHTML = htmlContent
  const imgs = contentBody.getElementsByTagName('img')
  if (imgs.length) {
    for (let img of imgs) {
      if (!img.dataset.face) {
        images.push({
          alt: img.alt,
          src: img.src
        })
      }
    }
  }
  return images
}

export function fixBoxSizing (list) {
  if (!list) {
    return
  }
  list.forEach(({elm}) => {
    if (elm && elm.nodeType === 1 && !/(^|\s+)el-.+/.test(elm.className)) {

    }
  })
}

export function throttle (fn, delay, alwayDelay) {
  var now, lastExec, timer, context, args; //eslint-disable-line

  var execute = function () {
    fn.apply(context, args)
    lastExec = now
  }

  return function () {
    context = this
    args = arguments

    now = Date.now()

    if (timer) {
      clearTimeout(timer)
      timer = null
    }

    if (lastExec || alwayDelay) {
      var diff = delay - (lastExec ? (now - lastExec) : 0)
      if (alwayDelay && diff < 0) {
        diff = delay
      }
      if (diff < 0) {
        execute()
      } else {
        timer = setTimeout(() => {
          execute()
        }, diff)
      }
    } else {
      execute()
    }
  }
}

export function request (config) {
  config = config || {}
  if (!config['headers']) {
    config.headers = {}
  }
  config.headers['X-Requested-With'] = 'XMLHttpRequest'
  config.headers['X-Saas'] = 'ajax'

  const instance = axios.create(config)
  instance.interceptors.response.use(res => {
    let data = res.data
    if (data && typeof data.status === 'number') {
      // 解析包含状态的返回值如：{"status": 200, "data": {}, "message":null}
      if (data.status !== 200) {
        return Promise.reject(data)
      } else {
        return data.hasOwnProperty('data') ? data.data : data
      }
    }
    return data
  }, err => {
    let status
    let error
    if (err.response) {
      status = err.response.status
      let data = err.response.data
      error = typeof data === 'object' ? {
        ...err,
        status,
        ...err.response.data
      } : {
        ...err,
        status,
        message: data
      }
      errorGoLogin(error)
    } else if (err.request) {
      status = err.request.status
    }
    return Promise.reject(error || {
      status,
      ...err
    })
  })
  return instance
}

export function getJsonDiff (o, n) {
  if (!o) {
    return { ...n }
  }
  let ret = {}
  // for (let p in n) {
  //   if (n.hasOwnProperty(p) && o[p] !== n[p]) {
  //     if (typeof o[p] === 'object' &&
  //         typeof n[p] === 'object' &&
  //         JSON.stringify(o[p]) === JSON.stringify(n[p])) {
  //       continue
  //     }
  //     ret[p] = n[p]
  //   }
  // }
  Object.keys(n).forEach(p => {
    if (o[p] !== n[p]) {
      if (typeof o[p] === 'object' &&
          typeof n[p] === 'object' &&
          JSON.stringify(o[p]) === JSON.stringify(n[p])) {
        return
      }
      ret[p] = n[p]
    }
  })
  return ret
}

export function buildUrl (url, params) {
  return url ? url.replace(/\{\{(\w+)\}\}/g, ($0, $1) => {
    return params && params.hasOwnProperty($1) ? params[$1] : ''
  }) : url
}

export function isElementInContainerView (el, container, partInView, rect) {
  let vheight = container.clientHeight
  let vwidth = container.clientWidth
  let crect = container.clientRect || container.getBoundingClientRect()
  rect = rect || el.getBoundingClientRect()
  let nr = {}
  nr.top = Math.floor(rect.top - crect.top)
  nr.left = Math.floor(rect.left - crect.left)
  nr.bottom = Math.floor(rect.bottom - crect.top)
  nr.right = Math.floor(rect.right - crect.left)
  nr.width = Math.floor(rect.width)
  nr.height = Math.floor(rect.height)
  return isElementInViewport(null, partInView, nr, vheight, vwidth)
}
// 判断元素是否在可视区内 代码参考 https://stackoverflow.com/a/7557433/5628
export function isElementInViewport (el, partInView, rect, vheight, vwidth) {
  // special bonus for those using jQuery
  // if (typeof jQuery === 'function' && el instanceof jQuery) {
  //   el = el[0]
  // }
  // let rect = el.getBoundingClientRect()
  rect = rect || el.getBoundingClientRect()
  vheight = vheight || window.innerHeight || document.documentElement.clientHeight
  vwidth = vwidth || window.innerWidth || document.documentElement.clientWidth
  if (partInView) {
    return (rect.bottom >= 0 && rect.right >= 0 &&
      rect.top <= vheight && rect.left <= vwidth)
  }
  return (rect.top >= 0 && rect.left >= 0 &&
      rect.bottom <= vheight && /* or $(window).height() */
      rect.right <= vwidth /* or $(window).width() */
  ) ||
    (rect.top <= 0 && rect.height > vheight && rect.top + rect.height > vheight) ||
    (rect.left <= 0 && rect.width > vwidth && rect.left + rect.width > vwidth)
}
export function onVisibilityChange (el, callback) {
  var oldVisible
  return function () {
    var visible = isElementInViewport(el)
    if (visible !== oldVisible) {
      if (typeof callback === 'function') {
        callback(visible, oldVisible)
      }
      oldVisible = visible
    }
  }
}

export function videoDuration (duration) {
  let sec = parseInt(duration || 0)
  let str = []
  if (sec > 60) {
    str.push(Math.floor(sec / 60))
    str.push(sec % 60)
  } else {
    str.push(0)
    str.push(sec)
  }
  let pad = '00'
  return str.map(i => {
    let s = '' + i
    return pad.substring(0, pad.length - s.length) + s
  }).join(':')
}

/**
 * https://stackoverflow.com/questions/27936772/how-to-deep-merge-instead-of-shallow-merge
 *
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export function isObject (item) {
  return (item && typeof item === 'object' && !Array.isArray(item))
}
/**
 * https://stackoverflow.com/questions/27936772/how-to-deep-merge-instead-of-shallow-merge
 *
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
export function mergeDeep (target, ...sources) {
  if (!sources.length) return target
  const source = sources.shift()

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} })
        mergeDeep(target[key], source[key])
      } else {
        Object.assign(target, { [key]: source[key] })
      }
    }
  }

  return mergeDeep(target, ...sources)
}

export const req = request()

const config = {
  headers: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
}
export const rest = {
  create (url, vo, cfg) {
    if (cfg) {
      cfg = mergeDeep({}, config, cfg)
    } else {
      cfg = config
    }
    return url ? req.post(wrapUrl(url), vo, cfg) : null
  },
  delete (url, params, traditional) {
    if (url && params) {
      let qsOptions = { allowDots: true }
      if (traditional) {
        qsOptions.indices = false
      }
      let ix = url.indexOf('?')
      url += (ix > -1 ? '&' : '?') + qs.stringify(params, qsOptions)
    }
    return url ? req.delete(wrapUrl(url)) : null
  },
  deleteByIds (url, ids) {
    if (url && ids) {
      let data = ids.reduce((ret, id) => {
        return (ret ? ret + '&' : '') + 'id=' + id
      }, '')
      let ix = url.indexOf('?')
      url += (ix > -1 ? '&' : '?') + data
    }
    return url ? req.delete(wrapUrl(url)) : null
  },
  edit (url, vo, cfg) {
    if (cfg) {
      cfg = mergeDeep({}, config, cfg)
    } else {
      cfg = config
    }
    return url ? req.put(wrapUrl(url), vo, cfg) : null
  },
  get (url, params, traditional) {
    if (url && params) {
      let qsOptions = { allowDots: true }
      if (traditional) {
        qsOptions.indices = false
      }
      let ix = url.indexOf('?')
      url += (ix > -1 ? '&' : '?') +
        (typeof params === 'string' ? params : qs.stringify(params, qsOptions))
    }
    return url ? req.get(wrapUrl(url)) : null
  }
}
rest.post = rest.create
rest.put = rest.edit

export default {
  install (Vue, options) {
    // 添加消息功能方法
    Object.assign(Vue.prototype, messagePrompt)
    // Vue.prototype.$bus = new Vue()

    let uid = 0
    Vue.mixin({
      beforeCreate () {
        // 添加组件唯一标识
        this.$uid = uid.toString()
        uid += 1
      }
    })
  }
}
