export const fullContent = state => {
  let mq = state.currMediaQuery
  let notLarge = mq === 'small' || mq === 'medium' || mq === 'pad'
  return notLarge && state.route.path && state.route.path.length > 1
}
