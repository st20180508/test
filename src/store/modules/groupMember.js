import { buildModule } from '@/store/module'
import { buildUrl, rest } from '@/util'

const { types, module } = buildModule({
  name: 'groupMember',
  restUrl: '/sns/groupMembers',
  restListQuery: 'sort=id,asc',
  types: {
    M_CHANGE_COMMOND_STATUS: 'M_CHANGE_COMMOND_STATUS',
    M_ADD_COMMOND_USED_LIST: 'M_ADD_COMMOND_USED_LIST',
    M_REMOVE_COMMOND_USED_LIST: 'M_REMOVE_COMMOND_USED_LIST',
    M_SET_COMMOND_USED_LIST: 'M_SET_COMMOND_USED_LIST',
    A_GET_COMMOND_USED_LIST: 'A_GET_COMMOND_USED_LIST',
    A_CHECK_GROUP_IS_COMMOND: 'A_GET_GROUP_IS_COMMOND',
    A_CHANGE_GROUP_COMMOND_USED: 'A_CHANGE_GROUP_COMMOND_USED',
    A_GET_GROUP_MEMBERS: 'A_GET_GROUP_MEMBERS',
    M_CHANGE_MEMBERS_LIST: 'M_CHANGE_MEMBERS_LIST',
    A_GET_GROUP_MANAGERS: 'A_GET_GROUP_MANAGERS',
    M_CHANGE_MANGERS_LIST: 'M_CHANGE_MANGERS_LIST',
    A_GET_GROUP_SUPERMANS: 'A_GET_GROUP_SUPERMANS',
    M_CHANGE_SUPERMAN_LIST: 'M_CHANGE_SUPERMAN_LIST',
    A_GET_GROUP_TYPES_MEMBER: 'A_GET_GROUP_TYPES_MEMBER',
    A_CHECK_JOINED_GROUP: 'A_CHECK_JOINED_GROUP',
    M_CHANGE_JOINED_STATUS: 'M_CHANGE_JOINED_STATUS',
    M_CHANGE_RELOAD_STATUS: 'M_CHANGE_RELOAD_STATUS',
    A_EXIT_GROUP: 'A_EXIT_GROUP',
    A_CHANGE_USER_IS_MANAGER: 'A_CHANGE_USER_IS_MANAGER',
    M_CHANGE_ADDMEMBER_DIALOG: 'M_CHANGE_ADDMEMBER_DIALOG'
  },
  state: {
    commondList: [],
    groupMembersList: [],
    groupManagersList: [],
    groupSupermanList: [],
    isCommondUsed: false,
    hasJoined: false,
    hasReload: false,
    listSize: 5,
    addMemberDialogStatus: false,
    addMemberDialogHeadTitle: '',
    addMemberDialogGroupId: ''
  }
})

Object.assign(module.mutations, {
  [ types.M_CHANGE_COMMOND_STATUS ] (state, bool) {
    state.isCommondUsed = bool
  },
  [ types.M_SET_COMMOND_USED_LIST ] (state, data) {
    state.commondList = data
  },
  [ types.M_ADD_COMMOND_USED_LIST ] (state, data) {
    state.commondList.splice(0, 0, data)
    if (state.commondList.length > state.listSize) {
      state.commondList.splice(state.commondList.length - 1, 1)
    }
  },
  [ types.M_REMOVE_COMMOND_USED_LIST ] (state, data) {
    for (var i = 0; i < state.commondList.length; i++) {
      if (state.commondList[i].id === data.id) {
        state.commondList.splice(i, 1)
        break
      }
    }
  },
  [ types.M_CHANGE_MEMBERS_LIST ] (state, data) {
    state.groupMembersList = data
  },
  [ types.M_CHANGE_MANGERS_LIST ] (state, data) {
    state.groupManagersList = data
  },
  [ types.M_CHANGE_SUPERMAN_LIST ] (state, data) {
    state.groupSupermanList = data
  },
  [ types.M_CHANGE_JOINED_STATUS ] (state, [data, ch]) {
    state.hasJoined = data
    if (ch) {
      state.hasReload = true
    }
  },
  [ types.M_CHANGE_RELOAD_STATUS ] (state, bool) {
    state.hasReload = bool
  },
  [ types.M_CHANGE_ADDMEMBER_DIALOG ] (state, data) {
    if (typeof data.status !== 'undefined') {
      state.addMemberDialogStatus = data.status
    }
    if (data.title) {
      state.addMemberDialogHeadTitle = data.title
    }
    if (data.groupId) {
      state.addMemberDialogGroupId = data.groupId
    }
  }
})
Object.assign(module.actions, {
  [ types.A_GET_COMMOND_USED_LIST ] ({ dispatch, commit, state }) {
    return rest.get('/sns/groupMembers/commonlyUsed')
      .then((data) => {
        commit(types.M_SET_COMMOND_USED_LIST, data)
      })
  },
  [ types.A_CHECK_GROUP_IS_COMMOND ] ({ dispatch, commit, state }, groupId) {
    return rest.get('/sns/groupMembers/' + groupId + '/isCommondUsed')
      .then((data) => commit(types.M_CHANGE_COMMOND_STATUS, data))
  },
  [ types.A_CHANGE_GROUP_COMMOND_USED ] ({ dispatch, commit, state }, groupId) {
    return rest.edit('/sns/groupMembers/' + groupId + '/commondUsed?size=' + state.listSize)
      .then((data) => {
        return data
      })
  },
  [ types.A_GET_GROUP_MEMBERS ] ({ dispatch, commit, state }, groupId) {
    return rest.get('/sns/groupMembers/groups/' + groupId + '/members')
    .then((data) => {
      commit(types.M_CHANGE_MEMBERS_LIST, data)
    })
  },
  [ types.A_GET_GROUP_MANAGERS ] ({ dispatch, commit, state }, groupId) {
    return rest.get('/sns/groupMembers/groups/' + groupId + '/manager')
    .then((data) => {
      commit(types.M_CHANGE_MANGERS_LIST, data)
    })
    .catch(() => commit(types.MSG, [ 'error', '查询失败!' ]))
  },
  [ types.A_GET_GROUP_SUPERMANS ] ({ dispatch, commit, state }, groupId) {
    return rest.get('/sns/groupMembers/groups/' + groupId + '/superman')
      .then((data) => {
        commit(types.M_CHANGE_SUPERMAN_LIST, data)
      })
      .catch(() => commit(types.MSG, [ 'error', '查询失败!' ]))
  },
  [ types.A_GET_GROUP_TYPES_MEMBER ] ({ dispatch, commit, state }, params) {
    return rest.get(buildUrl('/sns/groupMembers/groups/{{groupId}}?type={{type}}', params))
      .then((data) => {
        commit(types.M_CHANGE_MANGERS_LIST, data.manager)
        commit(types.M_CHANGE_MEMBERS_LIST, data.member)
        commit(types.M_CHANGE_SUPERMAN_LIST, data.superman)
        commit(types.M_CHANGE_RELOAD_STATUS, false)
      })
      .catch(() => commit(types.MSG, [ 'error', '查询失败!' ]))
  },
  [ types.A_CHECK_JOINED_GROUP ] ({ dispatch, commit, state }, groupId) {
    return rest.get('/sns/groupMembers/joined/' + groupId)
      .then((data) => {
        commit(types.M_CHANGE_JOINED_STATUS, [data, false])
        return data
      })
  },
  [ types.A_EXIT_GROUP ] ({ dispatch, commit, state }, groupId) {
    return rest.delete('/sns/groupMembers/exit/' + groupId)
      .then((data) => {
        commit(types.M_CHANGE_JOINED_STATUS, [false, true])
        return data
      })
  },
  [ types.A_CHANGE_USER_IS_MANAGER ] ({ dispatch, commit, state }, groupId) {
    return rest.get('/sns/groupMembers/isManager/' + groupId)
      .then((data) => {
        return data
      })
  }
})
export { types }
export default module
export const showAddMemberDialog = {
  get () {
    return this.$store.state.groupMember.addMemberDialogStatus
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_ADDMEMBER_DIALOG, {status: val})
  }
}
export const addMemberDialogTitle = {
  get () {
    return this.$store.state.groupMember.addMemberDialogHeadTitle
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_ADDMEMBER_DIALOG, {title: val})
  }
}
export const addMemberDialogGroupId = {
  get () {
    return this.$store.state.groupMember.addMemberDialogGroupId
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_ADDMEMBER_DIALOG, {groupId: val})
  }
}
