// import Vue from 'vue'
import { buildUrl, rest, getJsonDiff } from '@/util'
import qs from 'qs'

function delInArray (array, item) {
  let ix = array.indexOf(item)
  if (ix > -1) {
    array.splice(ix, 1)
  }
}

export function buildModule (options) {
  let name = options.name
  if (!name) {
    throw new Error('module name 不存在！')
  }
  let getId = options.getId || (vo => vo.id)

  let save = ({ commit, state }, id, nv) => {
    commit(types.ON_SAVE, true)
    return rest.edit(buildUrl(options.restEditUrl || options.restUrl + '/{{id}}', { id }), nv)
    .then((json) => {
      commit(types.M_UPDATE, json)
      commit(types.ON_SAVE, false)
      commit(types.MSG, [ 'success', '修改成功!' ])
      return json
    })
    .catch(e => {
      commit(types.ON_SAVE, false)
      commit(types.MSG, [ 'error', '修改失败!' ])
      return Promise.reject(e)
    })
  }

  let optionsTypes = {...options.types}
  Object.keys(optionsTypes).forEach(k => {
    optionsTypes[k] = name + '/' + optionsTypes[k]
  })
  const types = {
    A_CREATE: name + '/A_CREATE',
    A_DELETE: name + '/A_DELETE',
    A_DELETE_IDS: name + '/A_DELETE_IDS',
    A_FETCH_LIST: name + '/A_FETCH_LIST',
    A_GET: name + '/A_GET',
    A_GET_CURRENT: name + '/A_GET_CURRENT',
    A_SAVE_CURRENT: name + '/A_SAVE_CURRENT',
    A_SAVE: name + '/A_SAVE',
    A_SAVE_CHANGE: name + '/A_SAVE_CHANGE',
    M_CHANGE_CURRENT: name + '/M_CHANGE_CURRENT',
    M_CHANGE_PAGE: name + '/M_CHANGE_PAGE',
    M_PREVIOUS_PAGE: name + '/M_PREVIOUS_PAGE',
    M_NEXT_PAGE: name + '/M_NEXT_PAGE',
    M_UPDATE: name + '/M_UPDATE',
    M_SAVE: name + '/M_SAVE',
    M_DELETE: name + '/M_DELETE',
    M_SET_LIST: name + '/M_SET_LIST',
    M_ADD_SELECTED: name + '/M_ADD_SELECTED',
    M_DEL_SELECTED: name + '/M_DEL_SELECTED',
    M_SET_SELECTED: name + '/M_SET_SELECTED',
    M_SET_SEARCH: name + '/M_SET_SEARCH',
    M_SHOW_ITEM_PANEL: name + '/M_SHOW_ITEM_PANEL',
    M_SHOW_COLLECT_DIALOG: name + '/M_SHOW_COLLECT_DIALOG',
    M_SET_COLLECT_ID: name + '/M_SET_COLLECT_ID',
    ON_DELETE: name + '/ON_DELETE',
    ON_SAVE: name + '/ON_SAVE',
    ON_FETCH: name + '/ON_FETCH',
    ON_FETCH_LIST: name + '/ON_FETCH_LIST',
    MSG: name + '/MSG',
    ...optionsTypes
  }

  const state = {
    currItem: {},
    list: null,
    listSelected: [],
    page: {
      number: -1,
      size: 20,
      total: 0,
      first: false,
      last: false
    },
    search: {},
    statusDelete: false,
    statusSave: false,
    statusFetch: false,
    statusFetchList: false,
    showItemPanel: false,
    showCollectDialog: false,
    collectId: null,
    message: {},
    ...options.state
  }

  const getters = {
  }

  const mutations = {
    [ types.MSG ] (state, [ type, msg ]) {
      state.message = { type, msg }
    },
    [ types.M_CHANGE_CURRENT ] (state, vo) {
      if (getId(vo) === getId(state.currItem)) {
        // Vue.util.extend(state.currItem, vo)
        state.currItem = {...state.currItem, ...vo}
      } else {
        state.currItem = vo
      }
    },
    [ types.M_UPDATE ] (state, vo) {
      // Vue.util.extend(state.currItem, vo)
      // state.currItem = Object.assign({}, state.currItem, vo)
      state.currItem = {...state.currItem, ...vo}
    },
    [ types.M_SAVE ] (state, vo) {
      state.currItem = vo
    },
    [ types.M_SET_LIST ] (state, page) {
      state.list = page.content
      state.page.first = page.first
      state.page.last = page.last
      state.page.size = page.size
      state.page.total = page.totalElements
      state.page.number = page.number
    },
    [ types.M_CHANGE_PAGE ] (state, { page, size }) {
      if (size) {
        state.page.size = size
      }
      if (page) {
        state.page.number = page
      }
    },
    [ types.M_PREVIOUS_PAGE ] (state) {
      --state.page.number
    },
    [ types.M_NEXT_PAGE ] (state) {
      ++state.page.number
    },
    [ types.M_DELETE ] (state, { user }) {
      delInArray(state.list, user)
    },
    [ types.M_ADD_SELECTED ] (state, users) {
      if (Object.prototype.toString.call(users) === '[object Array]') {
        state.listSelected.push(...users)
      } else {
        state.listSelected.push(users)
      }
    },
    [ types.M_DEL_SELECTED ] (state, users) {
      if (Object.prototype.toString.call(users) === '[object Array]') {
        users.forEach((user) => {
          delInArray(state.listSelected, user)
        })
      } else {
        delInArray(state.listSelected, users)
      }
    },
    [ types.M_SET_SELECTED ] (state, users) {
      state.listSelected = users
    },
    [ types.M_SET_SEARCH ] (state, search) {
      state.search = search
    },
    [ types.M_SHOW_ITEM_PANEL ] (state, bool) {
      state.showItemPanel = bool
    },
    [ types.M_SHOW_COLLECT_DIALOG ] (state, bool) {
      state.showCollectDialog = bool
    },
    [ types.M_SET_COLLECT_ID ] (state, id) {
      if (id != null && id.trim().length > 0) {
        state.collectId = id
      }
    },
    [ types.ON_DELETE ] (state, bool) {
      state.statusDelete = bool
    },
    [ types.ON_SAVE ] (state, bool) {
      state.statusSave = bool
    },
    [ types.ON_FETCH ] (state, bool) {
      state.statusFetch = bool
    },
    [ types.ON_FETCH_LIST ] (state, bool) {
      state.statusFetchList = bool
    }
  }

  const actions = {
    [ types.A_CREATE ] ({ commit, state }, vo) {
      commit(types.ON_SAVE, true)
      return rest.create(options.restCreateUrl || options.restUrl, vo)
      .then((json) => {
        commit(types.M_SAVE, json)
        commit(types.ON_SAVE, false)
        commit(types.MSG, [ 'success', '创建成功!' ])
        return json
      })
      .catch(e => {
        commit(types.ON_SAVE, false)
        commit(types.MSG, [ 'error', '创建失败!' ])
        return Promise.reject(e)
      })
    },
    [ types.A_SAVE_CHANGE ] ({ commit, state }, { oldVal, newVal }) {
      let id = getId(oldVal)
      let nv = getJsonDiff(oldVal, newVal)
      return save({ commit, state }, id, nv)
    },
    [ types.A_SAVE ] ({ commit, state }, { id, item }) {
      return save({ commit, state }, id, item)
    },
    [ types.A_DELETE ] ({ dispatch, commit, state }, id) {
      commit(types.ON_DELETE, true)
      return rest.delete(buildUrl(options.restDeleteUrl || options.restUrl + '/{{id}}', { id }))
      .then(json => {
        commit(types.ON_DELETE, false)
        commit(types.MSG, [ 'success', '删除成功!' ])
        return json
      })
      .catch(e => {
        commit(types.ON_DELETE, false)
        commit(types.MSG, [ 'error', '删除失败!' ])
        return Promise.reject(e)
      })
    },
    [ types.A_DELETE_IDS ] ({ dispatch, commit, state }, ids) {
      let data = ids.reduce((ret, id) => {
        return (ret ? ret + '&' : '') + 'id=' + id
      }, '')
      commit(types.ON_DELETE, true)
      return rest.delete((options.restDeleteByIdsUrl || options.restUrl) + '?' + data)
      .then(json => {
        commit(types.ON_DELETE, false)
        commit(types.MSG, [ 'success', '批量删除成功!' ])
        return json
      })
      .catch(e => {
        commit(types.ON_DELETE, false)
        commit(types.MSG, [ 'error', '批量删除失败!' ])
        return Promise.reject(e)
      })
    },
    [ types.A_GET ] ({ commit, state }, id) {
      commit(types.ON_FETCH, true)
      return rest.get(buildUrl(options.restGetUrl || options.restUrl + '/{{id}}', { id }))
      .then(json => {
        commit(types.ON_FETCH, false)
        // commit(types.MSG, [ 'success', '获取成功!' ])
        return json
      })
      .catch(e => {
        commit(types.ON_FETCH, false)
        // commit(types.MSG, [ 'error', '获取失败!' ])
        return Promise.reject(e)
      })
    },
    [ types.A_SAVE_CURRENT ] ({ commit, state }, vo) {
      let id = getId(state.currItem)
      let nv = getJsonDiff(state.currItem, vo)
      return save({ commit, state }, id, nv)
      .then(json => {
        commit(types.M_CHANGE_CURRENT, JSON.parse(JSON.stringify(json)))
        return json
      })
    },
    [ types.A_GET_CURRENT ] ({ commit, state }, id) {
      return rest.get(buildUrl(options.restGetUrl || options.restUrl + '/{{id}}', { id }))
      .then((json) => {
        commit(types.ON_FETCH, false)
        commit(types.M_CHANGE_CURRENT, JSON.parse(JSON.stringify(json)))
        return json
      })
      .catch(e => {
        commit(types.ON_FETCH, false)
        return Promise.reject(e)
      })
    },
    [ types.A_FETCH_LIST ] (context, payload) {
      let { commit, state } = context
      commit(types.ON_FETCH_LIST, true)
      let params = { ...payload }
      params.page = state.page.number
      params.size = state.page.size
      params.search = qs.stringify(state.search, { allowDots: true, indices: false })
      let url = options.restGetAllUrl || (options.restUrl + '?{{search}}&page={{page}}&size={{size}}&' + (options.restListQuery || ''))
      let afterGet = options.afterFetchList || ((type, req) => req)
      return afterGet(context, rest.get(buildUrl(url, params)))
      .then((json) => {
        commit(types.M_SET_LIST, json)
        commit(types.ON_FETCH_LIST, false)
        return json
      })
      .catch(e => {
        commit(types.ON_FETCH_LIST, false)
        return Promise.reject(e)
      })
    }
  }

  return {
    options,
    module: {
      name,
      state,
      getters,
      actions,
      mutations
    },
    types
  }
}
