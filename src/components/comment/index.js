import md5 from 'js-md5'
/**
 * 通过评论数据组装文档或帖子标注的内容
 * @param comment
 * @returns 返回标注内容
 */
export function getAnnQuote (comment) {
  let quote = {}
  let comBody = comment.commentBody
  let type = comBody.commentQuote.quoteObjType
  quote.commentId = comment.id
  quote.id = comBody.commentQuote.quoteId || ''
  quote.objId = comBody.commentQuote.quoteObjId
  quote.objType = type === 3 ? 'doc' : 'post'
  quote.quote = comBody.commentQuote.quoteValue
  quote.text = comBody.content
  quote.ranges = comBody.commentQuote.quoteLocation
  quote.userId = comment.creator
  return quote
}

/**
 * 通过收藏或分享数据组装文档或帖子标注的内容
 * @param itemFrom item来源‘collect’,‘share’
 * @param item
 * @returns 返回标注内容
 */
export function getHighlightedQuote (itemFrom, item) {
  let quote = {}
  if (itemFrom === 'collect') { // ‘||’后面时来至新增收藏
    let colExtContent = item.colExtContent || JSON.parse(item.sourceExtContent)
    let type = item.sourceType === 3 ? 'doc' : 'post'
    quote.id = item.collectId || item.id
    quote.objId = item.source
    quote.objType = type
    quote.quote = colExtContent.quote
    quote.text = '收藏的内容'
    quote.ranges = colExtContent.ranges
    quote.userId = item.collector
  } else if (itemFrom === 'share') {
    let modelObject = item.modelObject
    let colExtContent = modelObject.quote
    let type = modelObject.refObjType
    quote.id = item.id
    quote.objId = modelObject.refObjId
    quote.objType = type === 3 ? 'doc' : 'post'
    quote.quote = colExtContent.quote
    quote.text = '分享的内容'
    quote.ranges = colExtContent.ranges
    quote.userId = item.creator
  } else if (itemFrom === 'card') {
    let colExtContent = item.colExtContent
    let type = item.refObjType
    quote.id = item.currPostId
    quote.objId = item.refObjId
    quote.objType = type === 3 ? 'doc' : 'post'
    quote.quote = colExtContent.quote
    // quote.text = item.shareContent
    quote.text = '分享的内容'
    quote.ranges = colExtContent.ranges
    quote.userId = item.currPostUserId
  }
  quote.permissions = {
    read: ['group:__self__'],
    admin: [],
    update: [],
    delete: []
  }
  return quote
}

export function gatherContentImages (content) {
  let files = []
  if (!content) {
    return files
  }
  let pasteBlock = document.createDocumentFragment()
  pasteBlock.appendChild(document.createElement('body'))
  let fragmentBody = pasteBlock.querySelector('body')
  fragmentBody.innerHTML = content
  let images = fragmentBody.getElementsByTagName('IMG')
  if (images && images.length > 0) {
    for (let img of images) {
      if (!img.getAttribute('data-storeId') && !img.getAttribute('data-face')) {
        let src = img.src
        let file = convert2ResourceFile(src)
        let check = files.filter(f => f.rsId === file.rsId)
        if (file && !(check && check.length > 0)) {
          files.push(file)
        }
      }
    }
  }
  return files
}

/**
 * 将外部图片路径转为当前可用的文件格式
 * @param src
 * @returns {{datas: {550x: *, 700x: *, 1000x: *, extInfo: {}}, name: *, objId: *, rsId: *, storeId: *, type: string}}
 */
export function convert2ResourceFile (src) {
  if (!src) {
    return null
  }
  let file = {
    datas: {
      '550x': src,
      '700x': src,
      '1000x': src,
      extInfo: {}
    },
    name: src,
    objId: src,
    rsId: md5(src),
    storeId: src,
    type: 'image'
  }
  return file
}
