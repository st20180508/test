import types from '@/store/mutation-types'
import { storeNeedData } from './dataPool'

export const currUser = function () {
  return this.$store.state.currUser || {}
}

export const currUserImg = function () {
  let currUser = this.$store.state.currUser
  if (currUser && currUser.avatar) {
    return currUser.avatar
  }
  return defUserImg(currUser ? currUser.sex : null)
}

export const defUserImg = function (sex) {
  return require('../../assets/img/man.png')
}

export const groupFormShow = {
  get () {
    return this.$store.state.group.showItemPanel
  },
  set (val) {
    this.$store.commit(types.group.M_SHOW_ITEM_PANEL, val)
  }
}

export const groupFormShowCreate = {
  get () {
    return this.$store.state.group.showItemCreatePanel
  },
  set (val) {
    this.$store.commit(types.group.M_SHOW_ITEM_CREATE_PANEL, val)
  }
}

export const postFormShow = {
  get () {
    return this.$store.state.post.showItemPanel
  },
  set (val) {
    this.$store.commit(types.post.M_SHOW_ITEM_PANEL, val)
  }
}

export const postFormShowCreate = {
  get () {
    return this.$store.state.post.showItemCreatePanel
  },
  set (val) {
    this.$store.commit(types.post.M_SHOW_ITEM_CREATE_PANEL, val)
  }
}
export const showCollect = {
  get () {
    return this.$store.state.post.showCollectDialog
  },
  set (val) {
    this.$store.commit(types.post.M_SHOW_COLLECT_DIALOG, val)
  }
}
export const currCollectId = {
  get () {
    return this.$store.state.post.collectId
  },
  set (val) {
    this.$store.commit(types.post.M_SET_COLLECT_ID, val)
  }
}
export default {
  install (Vue, options) {
    // 添加快捷功能方法
    Object.assign(Vue.prototype, {
      storeNeedData
    })
  }
}
