import { buildUrl, rest, getJsonDiff } from './index'
import qs from 'qs'

export default {
  computed: {
    initData () {
      return true
    },
    currItem () {
      return this.rest.currItem
    },
    items () {
      return this.rest.items
    },
    statusDelete () {
      return this.rest.status.delete
    },
    statusFetch () {
      return this.rest.status.fetch
    },
    statusFetchList () {
      return this.rest.status.fetchList
    },
    statusSave () {
      return this.rest.status.save
    },
    page: {
      get () {
        return this.rest.page.number + 1
      },
      set (val) {
        this.rest.page.number = val - 1
      }
    },
    pageInfo () {
      return this.rest.page
    },
    search: {
      get () {
        return this.rest.search
      },
      set (val) {
        this.rest.search = val
      }
    },
    restUrlPrefix () {
      return this.restUrl
    },
    restUrlCreate () {
      return this.restCreateUrl ? this.restCreateUrl : this.restUrlPrefix
    },
    restUrlDeleteByIds () {
      return this.restDeleteByIdsUrl ? this.restDeleteByIdsUrl : this.restUrlPrefix
    },
    restUrlDelete () {
      return this.restDeleteUrl ? this.restDeleteUrl : (this.restUrlPrefix + '/{{id}}')
    },
    restUrlEdit () {
      return this.restEditUrl ? this.restEditUrl : (this.restUrlPrefix + '/{{id}}')
    },
    restUrlGet () {
      return this.restGetUrl ? this.restGetUrl : (this.restUrlPrefix + '/{{id}}')
    },
    restUrlGetAll () {
      return this.restGetAllUrl ? this.restGetAllUrl : (this.restUrlPrefix + '?{{search}}&page={{page}}&size={{size}}&' + (this.restListQuery || ''))
    },
    listUrlParams () {
      return {}
    }
  },
  data () {
    return {
      rest: {
        currItem: {},
        items: null,
        page: {
          number: -1,
          size: 20,
          total: 0,
          first: false,
          last: false
        },
        search: {},
        message: {},
        status: {
          delete: false,
          save: false,
          fetch: false,
          fetchList: false
        }
      }
    }
  },
  props: {
    getItemKey: {
      type: Function,
      default (item) {
        return item ? item.id : null
      }
    },
    restUrl: String,
    restListQuery: String,
    restCreateUrl: String,
    restDeleteUrl: String,
    restDeleteByIdsUrl: String,
    restEditUrl: String,
    restGetUrl: String,
    restGetAllUrl: String
  },
  created () {
    if (this.initData) {
      this.getData()
    }
  },
  methods: {
    getData () {
      this.rest.status.fetchList = true
      return this.restGetAll({
        ...this.listUrlParams,
        page: this.rest.page.number,
        size: this.rest.page.size,
        search: qs.stringify(this.rest.search, { allowDots: true, indices: false })
      })
      .then(json => {
        this.rest.page.first = json.first
        this.rest.page.last = json.last
        this.rest.page.number = json.number
        this.rest.page.size = json.size
        this.rest.page.total = json.totalElements
        this.rest.items = json.content
        this.rest.status.fetchList = false
        return json
      })
      .catch(e => {
        this.rest.status.fetchList = false
        return Promise.reject(e)
      })
    },
    getItemByVal (val) {
      let id = ''
      let item = {}
      if (typeof val === 'string') {
        id = val
        this.items.some(i => {
          if (id === this.getItemKey(i)) {
            item = i
            return true
          }
          return false
        })
      } else {
        item = val
        id = this.getItemKey(item)
      }
      return {id, item}
    },
    setCurrItem (val) {
      this.rest.status.fetch = true
      let {id, item} = this.getItemByVal(val)
      this.rest.currItem = item
      return this.restGet(id)
      .then(json => {
        // Vue.util.extend(this.fetchCurrItem, json)
        this.rest.currItem = {...this.rest.currItem, ...json}
        return json
      })
    },
    saveCurrItem (item) {
      let nv = getJsonDiff(this.rest.currItem, item)
      return this.restEdit(this.getItemKey(this.rest.currItem), nv)
      .then(json => {
        // Vue.util.extend(this.fetchCurrItem, json)
        this.rest.currItem = {...this.rest.currItem, ...json}
        return json
      })
    },
    restCreate (vo) {
      this.rest.status.save = true
      return rest.create(this.restUrlCreate, vo)
      .then(json => {
        this.rest.status.save = false
        this.rest.message = {
          type: 'success',
          msg: '创建成功!'
        }
        return json
      })
      .catch(e => {
        this.rest.status.save = false
        this.rest.message = {
          type: 'error',
          msg: '创建失败!'
        }
        return Promise.reject(e)
      })
    },
    restDelete (id) {
      this.rest.status.delete = true
      return rest.delete(buildUrl(this.restUrlDelete, { id }))
      .then(json => {
        this.rest.status.delete = false
        this.rest.message = {
          type: 'success',
          msg: '删除成功!'
        }
        return json
      })
      .catch(e => {
        this.rest.status.delete = false
        this.rest.message = {
          type: 'error',
          msg: '删除失败!'
        }
        return Promise.reject(e)
      })
    },
    restDeleteByIds (ids) {
      this.rest.status.delete = true
      return rest.deleteByIds(this.restUrlDeleteByIds, ids)
      .then(json => {
        this.rest.status.delete = false
        this.rest.message = {
          type: 'success',
          msg: '批量删除成功!'
        }
        return json
      })
      .catch(e => {
        this.rest.status.delete = false
        this.rest.message = {
          type: 'error',
          msg: '批量删除失败!'
        }
        return Promise.reject(e)
      })
    },
    restEdit (id, vo) {
      this.rest.status.save = true
      return rest.edit(buildUrl(this.restUrlEdit, { id }), vo)
      .then(json => {
        this.rest.status.save = false
        this.rest.message = {
          type: 'success',
          msg: '修改成功!'
        }
        return json
      })
      .catch(e => {
        this.rest.status.save = false
        this.rest.message = {
          type: 'error',
          msg: '修改失败!'
        }
        return Promise.reject(e)
      })
    },
    restGet (id) {
      this.rest.status.fetch = true
      return rest.get(buildUrl(this.restUrlGet, { id }))
      .then(json => {
        this.rest.status.fetch = false
        return json
      })
      .catch(e => {
        this.rest.status.fetch = false
        return Promise.reject(e)
      })
    },
    restGetAll (params) {
      return this.restFilterGetAll(rest.get(buildUrl(this.restUrlGetAll, params)))
    },
    restFilterGetAll (req) {
      return req
    }
  }
}
