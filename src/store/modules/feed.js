import { rest, convertEmoji } from '@/util'
import Vue from 'vue'
import { storeNeedData, types as dataPoolTypes } from './dataPool'

function getScopeGroupId (feed) {
  if (!feed) {
    return
  }
  if (!feed.groupType || feed.groupType === 'group') {
    return feed.groupId
  } else if (feed.objContent && feed.objContent.scope === 0) { // 小组访问范围
    return feed.objContent.scopeValue
  }
}

export function fillFeed () {
}

export const types = {
  A_GET_FEED_LIST: 'A_GET_FEED_LIST',
  A_GET_FEED_UNREAD: 'A_GET_FEED_UNREAD',
  M_ADD_READ_FEED_ID_LIST: 'M_ADD_READ_FEED_ID_LIST',
  M_ADD_UNREAD_FEED_ID_LIST: 'M_ADD_UNREAD_FEED_ID_LIST',
  M_DEL_UNREAD_FEED_ID_LIST: 'M_DEL_UNREAD_FEED_ID_LIST',
  M_ADD_FEED_UNREAD_BY_KEY: 'M_ADD_FEED_UNREAD_BY_KEY',
  M_CLEAR_FEED_UNREAD_BY_KEY: 'M_CLEAR_FEED_UNREAD_BY_KEY',
  M_SET_FEED_UNREAD: 'M_SET_FEED_UNREAD'
}

const module = {
  name: 'feed',
  state: {
    readFeedIdList: [],
    feedUnreadIdList: {},
    feedUnread: {}
  },
  mutations: {
    [ types.M_ADD_UNREAD_FEED_ID_LIST ] (state, {key, ids}) {
      if (!state.feedUnreadIdList[key]) {
        Vue.set(state.feedUnreadIdList, key, [...ids])
      } else {
        state.feedUnreadIdList[key].push(...ids)
      }
      let lc = state.feedUnread[key]
      Vue.set(state.feedUnread, key, (lc || 0) + ids.length)
    },
    [ types.M_DEL_UNREAD_FEED_ID_LIST ] (state, {key, ids}) {
      let uil = state.feedUnreadIdList[key]
      if (uil) {
        let c = 0
        for (let i = 0, l = ids.length; i < l; i++) {
          let ix = uil.indexOf(ids[i])
          if (ix >= 0) {
            uil.splice(ix, 1)
            c++
          }
        }
        let lc = state.feedUnread[key]
        if (c && lc) {
          state.feedUnread[key] = lc - c || 0
        }
      }
    },
    [ types.M_ADD_READ_FEED_ID_LIST ] (state, ids) {
      state.readFeedIdList.push(...ids)
    },
    [ types.M_ADD_FEED_UNREAD_BY_KEY ] (state, {key, count}) {
      count = count ? parseInt(count) || 0 : 0
      let ov = state.feedUnread[key]
      if (ov) {
        state.feedUnread[key] = ov + count
      } else {
        Vue.set(state.feedUnread, key, count)
      }
    },
    [ types.M_CLEAR_FEED_UNREAD_BY_KEY ] (state, key) {
      let ov = state.feedUnread[key]
      if (ov) {
        state.feedUnread[key] = 0
      } else {
        Vue.set(state.feedUnread, key, 0)
      }
      if (state.feedUnreadIdList[key]) {
        state.feedUnreadIdList[key] = []
      }
    },
    [ types.M_SET_FEED_UNREAD ] (state, count) {
      state.feedUnread = count
    }
  },
  actions: {
    [ types.A_GET_FEED_LIST ] (context, ids, c, d) {
      return rest.get('/page/feeds/ids', {ids}, true)
      .then(feeds => {
        let us = context.rootState.dataPool.users
        let userIds = Object.keys(us)
        let users = feeds.filter(o => o && o.user && userIds.indexOf(o.user.id) < 0).map(o => o.user)
        if (users && users.length > 0) {
          context.commit(dataPoolTypes.M_ADD_USERS, users)
        }
        feeds.forEach(f => {
          if (f) {
            f.user = us[f.userId]
            let mo
            if (f.objContent && (mo = f.objContent.modelObject) && mo.content) {
              mo.content = convertEmoji(mo.content)
            }
          }
        })
        return storeNeedData({
          type: 'group',
          dataIds: feeds ? feeds.map(f => getScopeGroupId(f)).filter(o => !!o) : []
        }, context)
        .then(groups => {
          if (feeds) {
            feeds.forEach(f => {
              if (!f) {
                return
              }
              let sgId = getScopeGroupId(f)
              if (sgId) {
                f.scopeGroup = groups[sgId]
              }
              if (f.groupId && (!f.groupType || f.groupType === 'group')) {
                f.group = groups[f.groupId]
              }
            })
          }
          return feeds
        })
      })
    },
    [ types.A_GET_FEED_UNREAD ] ({ commit, state }) {
      rest.get('/page/feeds/unread')
      .then(data => {
        Object.keys(data).forEach(k => {
          let v = data[k]
          data[k] = v ? parseInt(v) || 0 : 0
        })
        commit(types.M_SET_FEED_UNREAD, data)
      })
    }
  },
  getters: {}
}

export default module
