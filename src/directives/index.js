import clickoutside from 'element-ui/lib/utils/clickoutside'
import infiniteScroll from 'vue-infinite-scroll'
import scrollOverLock from './scrollOverLock'

export {
  scrollOverLock,
  clickoutside,
  infiniteScroll
}

export const errorImg = {
  bind (el, binding, vnode, oldVnode) {
    let ei = el['@@errorImg'] = {
      type: 'error',
      getSrc () {
        let src
        if (binding.arg === 'user') {
          src = require('../assets/img/man.png')
        } else if (binding.arg === 'group') {
          src = require('../assets/img/1.jpg')
        } else if (binding.arg === 'video') {
          src = require('../assets/img/logo.png')
        } else {
          src = require('../assets/img/text.png')
        }
        return src
      },
      listener () {
        let src = ei.getSrc()
        if (src) {
          el.imgUrl = el.src
          el.src = src
        }
      }
    }
    if (!el.src) {
      el.src = ei.getSrc()
    }
    el.addEventListener(ei.type, ei.listener)
  },
  update (el, binding, vnode, oldVnode) {
    if (!el.src) {
      let ei = el['@@errorImg']
      if (ei) {
        el.src = ei.getSrc()
      }
    }
  },
  unbind (el) {
    let ei = el['@@errorImg']
    if (ei) {
      el.removeEventListener(ei.type, ei.listener, false)
      delete el['@@errorImg']
    }
  }
}
