import { buildModule } from '@/store/module'
import { rest, convertEmoji } from '@/util'

const {types, module} = buildModule({
  name: 'modData',
  types: {
    A_GET_MODULE_DATA: 'A_GET_MODULE_DATA',
    M_SET_MODULE_DATA: 'M_SET_MODULE_DATA'
  },
  state: {
    dataMapList: {}
  }
})

Object.assign(module.mutations, {
  [ types.M_SET_MODULE_DATA ] (state, {vo, key}) {
    state.dataMapList[key] = vo
  }
})

Object.assign(module.actions, {
  [ types.A_GET_MODULE_DATA ] ({dispatch, commit, state}, {url, key, type}) {
    commit(types.M_SET_MODULE_DATA, {vo: [], key: key})
    return rest.get(url)
    .then(json => {
      let list = []
      if (json && json.content) {
        list = json.content || []
      } else {
        list = json || []
      }
      if (type === 'post') {
        return rest.get('/page/feeds/objs', { id: list }, true)
        .then(json => {
          let feedList = []
          for (let i = 0; i < json.length; i++) {
            let feed = json[i]
            if (!feed) {
              continue
            }
            let mo
            if (feed.objContent && (mo = feed.objContent.modelObject) && mo.content) {
              mo.content = convertEmoji(mo.content)
            }
            let pd = JSON.parse(JSON.stringify(feed.objContent))
            pd.commentCount = feed.commentCoun || ''
            pd.praise = feed.praise ? feed.praise.length : 0
            feedList.push(pd)
          }
          list = feedList
          commit(types.M_SET_MODULE_DATA, {vo: list, key: key})
          return list
        })
      } else {
        commit(types.M_SET_MODULE_DATA, {vo: list, key: key})
        return list
      }
    })
  }
})
export {types}
export default module

