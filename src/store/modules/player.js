import { buildModule } from '@/store/module'

const {types, module} = buildModule({
  name: 'player',
  types: {
    M_CHANGE_CURRENT_VIDEO_PLAY_INFO: 'M_CHANGE_CURRENT_VIDEO_PLAY_INFO',
    M_CHANGE_CURRENT_VIDEO_CONTROL: 'M_CHANGE_CURRENT_VIDEO_CONTROL',
    M_CHANGE_CURRENT_VIDEO_CONTROL_COMMAND: 'M_CHANGE_CURRENT_VIDEO_CONTROL_COMMAND',
    M_CHANGE_CURRENT_VIDEO_CONTROL_PARAM: 'M_CHANGE_CURRENT_VIDEO_CONTROL_PARAM',
    M_CHANGE_CURRENT_VIDEO_CONTROL_RESULT: 'M_CHANGE_CURRENT_VIDEO_CONTROL_RESULT'
  },
  state: {
    currVideoPlayInfo: null,
    currVideoPlayControl: {
      command: '', // operControlCommand: play|stop  getValueCommand: getCurrTime
      param: '', // 控制传递的参数
      result: '' // 控制返回的结果
    }
  }
})

Object.assign(module.mutations, {
  [ types.M_CHANGE_CURRENT_VIDEO_PLAY_INFO ] (state, videoPlayInfo) {
    state.currVideoPlayInfo = videoPlayInfo
  },
  [ types.M_CHANGE_CURRENT_VIDEO_CONTROL ] (state, controlCommand) {
    state.currVideoPlayControl = controlCommand
  },
  [ types.M_CHANGE_CURRENT_VIDEO_CONTROL_COMMAND ] (state, command) {
    state.currVideoPlayControl.command = command
  },
  [ types.M_CHANGE_CURRENT_VIDEO_CONTROL_PARAM ] (state, param) {
    state.currVideoPlayControl.param = param
  },
  [ types.M_CHANGE_CURRENT_VIDEO_CONTROL_RESULT ] (state, result) {
    state.currVideoPlayControl.result = result
  }
})

export {types}
export default module

export const currVideoPlayInfo = {
  get () {
    return this.$store.state.player.currVideoPlayInfo
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_CURRENT_VIDEO_PLAY_INFO, val)
  }
}

export const currVideoPlayControl = {
  get () {
    return this.$store.state.player.currVideoPlayControl
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_CURRENT_VIDEO_CONTROL, val)
  }
}
