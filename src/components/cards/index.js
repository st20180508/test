export { default as ActivityCard } from './ActivityCard'
export { default as ArticleCard } from './ArticleCard'
export { default as AudioCard } from './AudioCard'
export { default as DocCard } from './DocCard'
export { default as GroupCard } from './GroupCard'
export { default as ImageCard } from './ImageCard'
export { default as MusicCard } from './MusicCard'
export { default as TextCard } from './TextCard'
export { default as VideoCard } from './VideoCard'
export { default as BlogCard } from './BlogCard'
export { default as NoneCard } from './NoneCard'
export { default as ReprintCard } from './ReprintCard'

export default function getCardItemViewName (cardData) {
  let item = cardData.item
  if (JSON.stringify(item) === '{}') {
    return 'none-card'
  } else if (cardData.type === 'group') {
    return 'group-card'
  } else if (cardData.type === 'post') {
    if (item.objType === 'POST') {
      if (item.objContent) {
        let modelObject = item.objContent.modelObject ? item.objContent.modelObject : item.objContent
        if (modelObject.contentType === 'normal') { // 普通帖子类型，包含：文本、图片、视频
          let postType = modelObject.postType
          if (postType === 'image') {
            return 'image-card'
          } else if (postType === 'video') {
            return 'video-card'
          } else if (postType === 'doc') {
            return 'doc-card'
          }
        } else if (modelObject.contentType === 'blog') {
          if (item.collectType === 2) {
            return 'image-card'
          } else if (item.collectType === 3) {
            return 'video-card'
          } else if (item.collectType === 4) {
            return 'doc-card'
          }
          return 'blog-card'
        } else if (modelObject.contentType === 'activity') {
          return 'activity-card'
        } else if (modelObject.contentType === 'reprint') {
          if (item.collectType === 2) {
            return 'image-card'
          } else if (item.collectType === 3) {
            return 'video-card'
          } else if (item.collectType === 4) {
            return 'doc-card'
          }
          return 'reprint-card'
        } else if (modelObject.contentType === 'share') {
          if (modelObject.refObjType === 1) {
            return 'image-card'
          } else if (modelObject.refObjType === 2) {
            return 'video-card'
          } else if (modelObject.refObjType === 3) {
            return 'doc-card'
          } else if (modelObject.refObjType === 6) {
            return 'blog-card'
          } else if (modelObject.refObjType === 13) {
            return 'reprint-card'
          }
        }
      }
      return 'text-card'
    }
    return 'text-card'
  } else if (cardData.type === 'activity') {
    return 'activity-card'
  } else if (cardData.type === 'reprint') {
    return 'reprint-card'
  } else if (cardData.type === 'image') {
    return 'image-card'
  } else if (cardData.type === 'video') {
    return 'video-card'
  } else if (cardData.type === 'doc') {
    return 'doc-card'
  }
}
