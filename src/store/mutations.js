import * as types from './mutation-types'

export default {
  [ types.M_SET_DOCUMENT_HIDDEN ] (state, bool) {
    state.documentHidden = bool
  },
  [ types.M_SET_CURR_USER ] (state, { user }) {
    state.currUser = user
  },
  [ types.M_SET_CURR_MEDIA_QUERY ] (state, mq) {
    state.currMediaQuery = mq
  },
  [ types.M_SIDEBAR_TOGGER ] (state) {
    state.showSidebar = !state.showSidebar
  },
  [ types.M_SIDEBAR_SMALL_TOGGER ] (state) {
    state.showSidebarInSmall = !state.showSidebarInSmall
  }
}
