export function MediumLightbox(element, options) {
  "use strict";
  if (!element) return;
  var zoomedImg;
  var isZoomed = false;
  var screenSize = {};
  var options = options || {};
  var margin = options.margin || 50;
  var scrollDiv = document.createElement("div");
  scrollDiv.className = "scrollbar-measure";
  document.body.appendChild(scrollDiv);
  var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);
  function updateScreenSize() {
    var w = window
      , d = document
      , e = d.documentElement
      , g = d.getElementsByTagName('body')[0]
      , x = w.innerWidth || e.clientWidth || g.clientWidth
      , y = w.innerHeight || e.clientHeight || g.clientHeight;
    screenSize.x = x;
    screenSize.y = y;
  }
  updateScreenSize();
  window.addEventListener("resize", updateScreenSize);
  var contain = document.body, images = document.createElement('img'), imgOverlay = document.createElement('span');
  var imgH, imgW, imgL, imgT
  function zoom(e) {
    var $this = e.target
    if (!isZoomed) {
      isZoomed = !isZoomed;
      zoomedImg = $this;
      if (!$this.img)
        $this.img = $this || $this.getElementsByTagName('img')[0];
      imgH = $this.img.getBoundingClientRect().height;
      imgW = $this.img.getBoundingClientRect().width;
      imgL = $this.img.getBoundingClientRect().left;
      imgT = $this.img.getBoundingClientRect().top;
      var realW, realH;
      if ($this.img.dataset) {
        realW = $this.img.dataset.width || $this.naturalWidth;
        realH = $this.img.dataset.height || $this.naturalHeight;
      }
      images.src = $this.getAttribute('src')
      images.style.cssText = 'height:' + imgH + 'px; width: ' + imgW + 'px; left:' + imgL +'px; top:' + imgT + 'px;';

      $this.overlay = document.createElement('div');
      $this.overlay.id = 'the-overlay';
      $this.overlay.className = 'zoomOverlay';

      imgOverlay.appendChild(images)

      $this.wrapper = document.createElement('div');
      $this.wrapper.id = 'the-wrapper';
      $this.wrapper.className = 'zoomImg-wrap zoomImg-wrap--absolute';

      $this.wrapper.appendChild(imgOverlay);
      $this.wrapper.appendChild($this.overlay);
      contain.appendChild($this.wrapper);
      $this.wrapper.addEventListener('click', zoomOut)
      var wrapX = (screenSize.x - scrollbarWidth - imgW )/ 2 - imgL;
      var wrapY = (screenSize.y - imgH) / 2 - imgT;
      var scale = 1;
      if (realH > imgH) {
        if (imgH == imgW && screenSize.y > screenSize.x) {
          scale = (screenSize.x - margin) / imgW;
        } else if (imgH == imgW && screenSize.y < screenSize.x) {
          scale = (screenSize.y - margin) / imgH;
        } else if (imgH > imgW) {
          scale = (screenSize.y - margin) / imgH;
          if (scale * imgW > screenSize.x) {
            scale = (screenSize.x - margin) / imgW;
          }
          ;
        } else if (imgH < imgW) {
          scale = (screenSize.x - margin) / imgW;
          if (scale * imgH > screenSize.y) {
            scale = (screenSize.y - margin) / imgH;
          }
          ;
        }
      }
      if (scale * imgW > realW) {
        scale = realW / imgW;
        console.log('big')
      }
      var that = $this;
      setTimeout(function() {
        imgOverlay.style.cssText = 'height:' + imgH + 'px; width: ' + imgW + 'px; left:' + imgL +'px; top:' + imgT + 'px;' +
          'transform: translate(' + wrapX + 'px,' + wrapY + 'px) translateZ(0px);-webkit-transform: translate(' + wrapX + 'px,' + wrapY + 'px) translateZ(0px)';
        that.overlay.className = 'zoomOverlay show';
        images.style.cssText = 'transform:scale(' + scale + ')';
      }, 0);
    } else {
      isZoomed = !isZoomed;
      zoomedImg = null
      imgOverlay.style.cssText = 'height:' + imgH + 'px; width: ' + imgW + 'px; left:' + imgL +'px; top:' + imgT + 'px;';
      images.style.cssText = '';
      $this.overlay.className = 'zoomOverlay';
      var that = $this;
      setTimeout(function() {
        contain.removeChild(that.wrapper)
        imgL = null; imgT = null;
        imgOverlay.style.cssText = '';
      }, 300)
    }
  }
  var elements = document.querySelector(element);
  elements.addEventListener("click", function (e) {
    if (e.target.tagName === 'IMG') {
      console.log('111')
      zoom(e)
    }
  });
  return elements


  function zoomOut() {
    if (zoomedImg) {
      zoomedImg.click();
    }
  }
  window.addEventListener("scroll", zoomOut);
}
