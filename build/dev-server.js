require('./check-versions')()

var config = require('../config')
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
}

var opn = require('opn')
var path = require('path')
var express = require('express')
var webpack = require('webpack')
var proxyMiddleware = require('http-proxy-middleware')
var webpackConfig = process.env.NODE_ENV === 'testing'
  ? require('./webpack.prod.conf')
  : require('./webpack.dev.conf')

var session = require('express-session')

// default port where dev server listens for incoming traffic
var port = process.env.PORT || config.dev.port
// automatically open browser, if not set will be false
var autoOpenBrowser = !!config.dev.autoOpenBrowser
// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
var proxyTable = config.dev.proxyTable

var app = express()
var compiler = webpack(webpackConfig)

var devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: webpackConfig.output.publicPath,
  quiet: true
})

var hotMiddleware = require('webpack-hot-middleware')(compiler, {
  log: () => {}
})
// force page reload when html-webpack-plugin template changes
compiler.plugin('compilation', function (compilation) {
  compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
    hotMiddleware.publish({ action: 'reload' })
    cb()
  })
})

if (config.multiPageRoot) {
  var utils = require('./utils')
  var entries = utils.getEntries(config.multiPageRoot)
  if (entries.length > 0) {
    var paths = entries.reduce((r, en) => {
      r[en.name] = `/${en.filename}`
      return r
    }, {})
    var regex = /^\/(.+?)([\/\?].*)?$/
    app.use((req, res, next) => {
      var match = regex.exec(req.url) // 获取多页面前缀如： /xxx/yyy?aa=bb ---> xxx
      if (match && match[1]) {
        var url = paths[match[1]]
        if (url) {
          var idx = req.url.indexOf('?')
          req.url = idx > 0 ? url + req.url.substring(idx) : url
        }
      }
      next()
    })
  }
}

var sessionStore = new session.MemoryStore()
var sessionOptions = {
  resave: false,
  saveUninitialized: true,
  secret: 'keyboard cat',
  cookie: {},
  store: sessionStore
}
app.use(session(sessionOptions))

var useOauthInDev = process.env.OAUTH === 'true'
var wsProxy
if (useOauthInDev) {
  var getOauth2data = function (session) {
    var oauth2data = session ? session.oauth : null
    if (!oauth2data) {
      console.error('oauth2 data not found');
      return
    }
    if (!oauth2data.token) {
      console.error('oauth2 data token not found');
      return
    }
    return oauth2data
  }

  if (config.devo.proxyTable) {
    proxyTable = config.devo.proxyTable
  }
  var cookie = require('cookie');
  var cookieSignature = require('cookie-signature');
  Object.keys(proxyTable).forEach(function (context) {
    var options = proxyTable[context]
    if (options.ws) {
      options.onProxyReqWs = function (proxyReq, req, socket, options, head) {
        var cookies = cookie.parse(req.headers.cookie);
        var cid = cookies["connect.sid"]
        var sid
        if (cid) {
          sid = cookieSignature.unsign(cid.substr(0, 2) === 's:' ? cid.slice(2) : cid, sessionOptions.secret)
        }
        if (sid && sid.length < req.headers.cookie.length) {
          var sess = sessionStore.sessions[sid]
          console.log(11, sid, 22, sess, sessionStore.sessions)
          var session = JSON.parse(sess)
          var oauth2data = getOauth2data(session)
          if (oauth2data) {
            proxyReq.setHeader('access-token', oauth2data.token.access_token);
          }
        }
      }
    } else {
      options.onProxyReq = function (proxyReq, req, res) {
        var oauth2data = getOauth2data(req.session)
        if (oauth2data) {
          proxyReq.path = proxyReq.path + (proxyReq.path.indexOf('?') > 0 ? '&' : '?') + 'access_token=' + oauth2data.token.access_token
          if (oauth2data.headers) {
            Object.keys(oauth2data.headers).forEach(k => {
              proxyReq.setHeader(k, oauth2data.headers[k])
            })
          }
        }
      }
    }
  })

  // Set the configuration settings
  // Initialize the OAuth2 Library
  var oauth2 = require('simple-oauth2').create(config.devo.oauth);

  // Callback service parsing the authorization token and asking for the access token
  app.get('/oauth_callback', (req, res) => {
    var oauth2data = req.session.oauth
    var headers = req.query.headers
    if (headers) {
      try {
        oauth2data.headers = JSON.parse(headers)
      } catch (e) {
        console.log('set user header error', e)
      }
    }
    var code = req.query.code
    if (!code) {
      res.end()
      return
    }
    oauth2data.code = code;
    var tokenConfig = {
      code: code,
      redirect_uri: oauth2data.redirect_uri
    };

    oauth2.authorizationCode.getToken(tokenConfig, (error, result) => {
      if (error) {
        console.error('Access Token Error', error.message);
        return res.json('Authentication failed');
      }

      console.log('The resulting token: ', result);
      var token = oauth2.accessToken.create(result);
      oauth2data.token = token.token

      res.redirect(req.session.originalUrl || '/')
    });
  });
  app.get('/logout', (req, res) => {
    req.session.oauth = null;
    let host = config.devo.oauth.auth.tokenHost
    let url = host + '/logout?redirect=' + encodeURIComponent(req.query.redirect)
    res.redirect(url);
  });
  app.use((req, res, next) => {
    if (req.url !== '/__webpack_hmr' && req.url.indexOf('/dev/') !== 0
        && (req.url === '/' || /^.*?\/\w+$/.test(req.url))) {
      var oauth2data = req.session.oauth
      if (!oauth2data) {
        oauth2data = {}
        req.session.oauth = oauth2data
      }
      if (!oauth2data.code) {
        req.session.originalUrl = req.originalUrl
        // Authorization oauth2 URI
        oauth2data.redirect_uri = req.protocol + '://' + req.get('host') + '/oauth_callback'
        var authorizationUri = oauth2.authorizationCode.authorizeURL({
          redirect_uri: oauth2data.redirect_uri,
          scope: 'app',
          state: 'nodejsvue'
        });
        res.redirect(authorizationUri)
        return
      }
    }

    next()
  })
}


// proxy api requests
Object.keys(proxyTable).forEach(function (context) {
  var options = proxyTable[context]
  if (typeof options === 'string') {
    options = { target: options }
  }
  app.use(proxyMiddleware(options.filter || context, options))
})

// handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')())

// serve webpack bundle output
app.use(devMiddleware)

// enable hot-reload and state-preserving
// compilation error display
app.use(hotMiddleware)

// serve pure static assets
var staticPath = path.posix.join(config.dev.assetsPublicPath, config.dev.assetsSubDirectory)
app.use(staticPath, express.static('./static'))

var uri = 'http://localhost:' + port

var _resolve
var readyPromise = new Promise(resolve => {
  _resolve = resolve
})

console.log('> Starting dev server...')
devMiddleware.waitUntilValid(() => {
  console.log('> Listening at ' + uri + '\n')
  // when env is testing, don't need open it
  if (autoOpenBrowser && process.env.NODE_ENV !== 'testing') {
    opn(uri)
  }
  _resolve()
})

var server = app.listen(port)

if (wsProxy) {
  server.on('upgrade', wsProxy.upgrade)
}

module.exports = {
  ready: readyPromise,
  close: () => {
    server.close()
  }
}
