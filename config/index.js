// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')
var env = require('./prod.env')

module.exports = {
  multiPageRoot: path.resolve(__dirname, '../src/pages'),
  build: {
    env: env,
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: env.BASE_PATH.replace(/"/g, ''),
    productionSourceMap: true,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: require('./dev.env'),
    port: 3000,
    autoOpenBrowser: true,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      '/dev': {
        target: 'http://192.168.16.189/',
        // target: 'http://192.168.16.189/page',
        changeOrigin: true,
        headers: {
          tid: '0001',
          uid: '000000000001',
          Cookie: 'sid=eed96912-1550-40c7-9e9e-883d57984953'
        },
        pathRewrite: {
          '^/dev': ''
        }
      }
    },
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false
  },
  devo: {
    oauth: {
      client: {
        id: 'acme',
        secret: 'acmesecret'
      },
      auth: {
        tokenHost: 'http://saas.work.net',
        tokenPath: '/oauth/oauth/token',
        authorizePath: '/oauth/oauth/authorize'
      }
    },
    proxyTable: {
      '/ws': {
        target: 'http://saas.work.net',
        changeOrigin: true,
        ws: true
      },
      '/dev/cms': {
        target: 'http://cms.work.net/',
        changeOrigin: true,
        pathRewrite: {
          '^/dev/cms': ''
        }
      },
      '/dev/hdds': {
        target: 'http://192.168.16.111:10080/',
        changeOrigin: true,
        pathRewrite: {
          '^/dev/hdds': '/hdds'
        }
      },
      '/dev': {
        target: 'http://saas.work.net',
        changeOrigin: true,
        pathRewrite: {
          '^/dev': ''
        }
      }
    }
  }

}
