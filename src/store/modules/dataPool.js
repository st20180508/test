import Vue from 'vue'
import { buildModule } from '@/store/module'
import { rest } from '@/util'
import qs from 'qs'

const { types, module } = buildModule({
  name: 'dataPool',
  types: {
    A_NEED_GROUPS: 'A_NEED_GROUPS',
    A_NEED_USERS: 'A_NEED_USERS',
    M_ADD_GROUPS: 'M_ADD_GROUPS',
    M_ADD_USERS: 'M_ADD_USERS'
  },
  state: {
    groups: {},
    users: {}
  }
})

function copyData (from, to) {
  Object.keys(from).forEach(k => {
    if (to[k]) {
      to[k] = from[k]
    } else {
      Vue.set(to, k, from[k])
    }
  })
}

Object.assign(module.mutations, {
  [ types.M_ADD_GROUPS ] (state, data) {
    if (data) {
      data.forEach(o => {
        if (o && o.id) {
          let ov = state.groups[o.id]
          if (ov) {
            copyData(o, ov)
          } else {
            Vue.set(state.groups, o.id, o)
          }
        }
      })
    }
  },
  [ types.M_ADD_USERS ] (state, data) {
    if (data) {
      // 解析extInfo
      if (data.extInfo) {
        data.extInfoObject = JSON.parse(data.extInfo)
      } else {
        data.extInfoObject = {}
      }
      data.forEach(o => {
        if (o && o.id) {
          let ov = state.users[o.id]
          if (ov) {
            copyData(o, ov)
          } else {
            Vue.set(state.users, o.id, o)
          }
        }
      })
    }
  }
})

Object.assign(module.actions, {
  [ types.A_NEED_GROUPS ] ({ dispatch, commit, state }, ids) {
    if (ids) {
      let dataIds = Object.keys(ids.filter(id => id && !state.groups[id]).reduce((v, id) => {
        v[id] = false
        return v
      }, {}))
      if (dataIds && dataIds.length > 0) {
        return rest.get('/page/datapool/alias/groups', { dataIds }, true)
        .then(data => {
          commit(types.M_ADD_GROUPS, data)
          return state.groups
        })
      }
    }
    return Promise.resolve(state.groups)
  },
  [ types.A_NEED_USERS ] ({ dispatch, commit, state }, ids) {
    if (ids) {
      let dataIds = Object.keys(ids.filter(id => id && !state.users[id]).reduce((v, id) => {
        v[id] = false
        return v
      }, {}))
      if (dataIds && dataIds.length > 0) {
        return rest.get('/page/datapool/alias/users', qs.stringify({ dataIds }, { indices: false }))
        .then(data => {
          commit(types.M_ADD_USERS, data)
          return state.users
        })
      }
    }
    return Promise.resolve(state.users)
  }
})

const getTypeName = function (type) {
  let name
  if (type === 'group') {
    name = types.A_NEED_GROUPS
  } else if (type === 'user') {
    name = types.A_NEED_USERS
  }
  return name
}

const storeNeedData = function (ids, context) {
  if (ids) {
    let dispatch = this && this.$store ? this.$store.dispatch : context.dispatch
    if (Array.isArray(ids)) {
      if (ids.length > 0) {
        let promises = ids.map(({ type, dataIds }) => {
          let name = getTypeName(type)
          return name ? dispatch(name, dataIds) : Promise.resolve({})
        })
        return Promise.all(promises)
        .then(list => {
          list.forEach((o, i) => {
            list[ids[i].type] = o
          })
          return list
        })
      }
      return Promise.all([])
    } else if (ids.type) {
      let name = getTypeName(ids.type)
      return name ? dispatch(name, ids.dataIds) : Promise.resolve({})
    }
  }
  return Promise.resolve({})
}

export {
  types,
  storeNeedData
}
export default module
