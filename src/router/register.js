import Vue from 'vue'
import Router from 'vue-router'
import { basePath } from '@/util'

Vue.use(Router)

const routes = [{
  path: '/register',
  component: require('@/views/register/CoRegister')
}, {
  path: '/register/feedback',
  name: 'feedback',
  component: require('@/views/register/Feedback')
}, {
  path: '/register/initialize',
  name: 'initialize',
  component: require('@/views/register/Initialize')
}]

export default new Router({
  base: basePath,
  routes,
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
