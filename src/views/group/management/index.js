export { default as TextItem } from './TextItem'
export { default as ImageItem } from './ImageItem'
export { default as VideoItem } from './VideoItem'
export { default as DocItem } from './DocItem'
export { default as ShareItem } from './ShareItem'
export { default as BlogItem } from './BlogItem'
export { default as ReprintItem } from './ReprintItem'
export { default as ActivityItem } from './ActivityItem'
export { default as AudioItem } from './AudioItem'
export { default as MusicItem } from './MusicItem'

export default function getItemView (item) {
  let view = {
    name: 'text-item',
    title: '帖子'
  }
  if (item.objType === 'POST') {
    if (item.objContent) {
      let modelObject = item.objContent.modelObject ? item.objContent.modelObject : item.objContent
      if (modelObject.contentType === 'normal') { // 普通帖子类型，包含：文本、图片、视频
        let postType = modelObject.postType
        if (postType === 'image') {
          view.name = 'image-item'
          view.title = '图片'
        } else if (postType === 'video') {
          view.name = 'video-item'
          view.title = '视频'
        } else if (postType === 'doc') {
          view.name = 'doc-item'
          view.title = '文档'
        }
      } else if (modelObject.contentType === 'share') {
        view.name = 'share-item'
        view.title = '分享'
      } else if (modelObject.contentType === 'blog') {
        view.name = 'blog-item'
        view.title = '博文'
      } else if (modelObject.contentType === 'reprint') {
        view.name = 'reprint-item'
        view.title = '转载'
      } else if (modelObject.contentType === 'activity') {
        view.name = 'activity-item'
        view.title = '活动'
      }
    }
  }
  return view
}
