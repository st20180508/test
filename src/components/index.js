import { camelToKebab } from '@/util'
import Mod from '@/components/Mod'
import PeopleList from '@/components/PeopleList'
import FileIcon from '@/components/FileIcon'
import ListLoading from '@/components/ListLoading'

export const components = {
  Mod,
  PeopleList,
  FileIcon,
  ListLoading
}

export default {
  install (Vue, opts = {}) {
    // Object.values(components).forEach(comp => Vue.component(comp.name, comp))
    Object.entries(components).forEach(([k, v]) => Vue.component(camelToKebab(k), v))
    // for (let [k, v] of Object.entries(components)) {
    //   Vue.component(camelToKebab(k), v)
    // }
  }
}
