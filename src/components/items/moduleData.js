export function getGroupPathOption (item) {
  let option = {
    name: 'group',
    params: {
      groupId: item.id
    }
  }
  return option
}
export function getGroupOtherHtml (item) {
  return '组员 ' + item.memberCount + '人'
}
export function getPostPathOption (item) {
  let option = {
    name: 'post',
    params: {
      id: item.id
    }
  }
  return option
}
export function getPostOtherHtml (item) {
  let html = `<a><i class="fa fa-heart-o"></i> ` + (item && item.praise ? item.praise : '') + `</a>` +
    `<a><i class="fa fa-comment-o"></i> ` + (item && item.commentCount ? item.commentCount : '') + `</a>`
  return html
}
export function getUserPathOption (item) {
  let option = {
    name: 'user',
    params: {
      userId: item ? item.id : null
    }
  }
  return option
}
