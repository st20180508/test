import 'es6-promise/auto'

import '@/assets/element/theme/index.css'
import Element from 'element-ui'
import '../../static/css/font-awesome.min.css'

import Vue from 'vue'
import App from '@/views/register/Index'
import router from '@/router/register'

// 引入“饿了么”界面组件
Vue.use(Element)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
