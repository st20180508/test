export { default as ActivityCard } from '@/components/cards/ActivityCard'
export { default as ArticleCard } from '@/components/cards/ArticleCard'
export { default as AudioCard } from '@/components/cards/AudioCard'
export { default as DocCard } from '@/components/cards/DocCard'
export { default as GroupCard } from '@/components/cards/GroupCard'
export { default as ImageCard } from '@/components/cards/ImageCard'
export { default as MusicCard } from '@/components/cards/MusicCard'
export { default as TextCard } from '@/components/cards/TextCard'
export { default as VideoCard } from '@/components/cards/VideoCard'
export { default as BlogCard } from '@/components/cards/BlogCard'
export { default as ReprintCard } from '@/components/cards/ReprintCard'
export { default as NoneCard } from '@/components/cards/NoneCard'
export { default as ParticipleCard } from '@/components/cards/ParticipleCard'
export { default as ShareCard } from './ShareCard'

export default function getCardItemViewName (cardData) {
  let item = cardData.item
  if (JSON.stringify(cardData.item) === '{}') {
    return 'none-card'
  } else if (cardData.type === 'group') {
    return 'group-card'
  } else if (cardData.type === 'post') {
    if (item.objType === 'POST') {
      if (item.objContent) {
        let modelObject = item.objContent.modelObject ? item.objContent.modelObject : item.objContent
        if (modelObject.contentType === 'normal') { // 普通帖子类型，包含：文本、图片、视频
          let postType = modelObject.postType
          if (postType === 'image') {
            return 'image-card'
          } else if (postType === 'video') {
            return 'video-card'
          } else if (postType === 'doc') {
            return 'doc-card'
          }
        } else if (modelObject.contentType === 'blog') {
          if (item.collectType === 2) {
            return 'image-card'
          } else if (item.collectType === 3) {
            return 'video-card'
          } else if (item.collectType === 4) {
            return 'doc-card'
          }
          return 'blog-card'
        } else if (modelObject.contentType === 'activity') {
          return 'activity-card'
        } else if (modelObject.contentType === 'reprint') {
          if (item.collectType === 2) {
            return 'image-card'
          } else if (item.collectType === 3) {
            return 'video-card'
          } else if (item.collectType === 4) {
            return 'doc-card'
          }
          return 'reprint-card'
        } else if (modelObject.contentType === 'share') {
          return 'share-card'
        }
      }
      return 'text-card'
    }
    return 'text-card'
  } else if (cardData.type === 'quote') {
    return 'participle-card'
  } else if (cardData.type === 'activity') {
    return 'activity-card'
  }
}
