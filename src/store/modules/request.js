import { buildModule } from '@/store/module'
import { buildUrl, rest } from '@/util'

const { types, module } = buildModule({
  name: 'request',
  restUrl: '/sns/requests',
  types: {
    A_GET_COUNT: 'A_GET_COUNT',
    A_AUDIT_REQUEST: 'A_AUDIT_REQUEST'
  },
  state: {
    countUrl: '/sns/requests/audit/count?createAt={{createAt}}&type={{type}}&createAtType={{createAtType}}',
    auditUrl: '/sns/requests/audit?requestId={{requestId}}&type={{type}}&auditType={{auditType}}'
  }
})

Object.assign(module.mutations, {
})

Object.assign(module.actions, {
  [ types.A_GET_COUNT ] ({ commit, state }, params) {
    return rest.get(buildUrl(state.countUrl, params))
      .then((json) => {
        return json
      })
      .catch(e => {
        return Promise.reject(e)
      })
  },
  [ types.A_AUDIT_REQUEST ] ({commit, state}, params) {
    return rest.create(buildUrl(state.auditUrl, params))
  }
})
export { types }
export default module
