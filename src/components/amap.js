import Vue from 'vue'

let VueAMap
function promiseAMapComponent (comp) {
  return () => {
    if (VueAMap) {
      return Promise.resolve(VueAMap[comp])
    } else {
      return import(/* webpackChunkName: "amap" */ 'vue-amap').then(vam => {
        if (!VueAMap) {
          // 模拟Vue并代替插件初始化
          vam.default.install({
            config: JSON.parse(JSON.stringify(Vue.config)),
            component () {}
          }, {})
          // vue-amap初始化
          vam.initAMapApiLoader(config)
          VueAMap = vam.default
        }
        return VueAMap[comp]
      })
    }
  }
}

export const config = {
  // 高德的key
  key: '9d895faf409f9bf81b46b01a6f3f3d72',
  // key: '35321a87960786d66958d1ac6656683d',
  // 插件集合
  plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor', 'Geocoder', 'Geolocation']
}

export const Amap = promiseAMapComponent('Amap')
export const AmapMarker = promiseAMapComponent('AmapMarker')
export const AmapSearchBox = () => promiseAMapComponent('AmapSearchBox')().then(Comp => {
  return {
    extends: Comp,
    props: {
      value: String,
      showBtn: true
    },
    watch: {
      value (nv, ov) {
        if (this.keyword !== nv) {
          this.keyword = nv
        }
      },
      keyword (nv, ov) {
        this.$emit('keywordChange', nv, ov)
        if (this.value !== nv) {
          this.$emit('input', nv)
        }
      }
    },
    mounted () {
      if (this.value !== this.keyword) {
        this.keyword = this.value
      }
      if (!this.showBtn) {
        let el = this.$el.querySelector('.search-btn')
        if (el) {
          el.style.display = 'none'
        }
      }
    },
    methods: {
      changeTip (tip) {
        this.keyword = tip.name
        this.$emit('selected', tip)
        this.search()
      },
      selectTip (type) {
        if (type === 'up' && this.selectedTip > 0) {
          this.selectedTip -= 1
          this.keyword = this.tips[this.selectedTip].name
          this.$emit('selected', this.tips[this.selectedTip])
        } else if (type === 'down' && this.selectedTip + 1 < this.tips.length) {
          this.selectedTip += 1
          this.keyword = this.tips[this.selectedTip].name
          this.$emit('selected', this.tips[this.selectedTip])
        }
      }
    }
  }
})
export const AmapCircle = promiseAMapComponent('AmapCircle')
export const AmapGroundImage = promiseAMapComponent('AmapGroundImage')
export const AmapInfoWindow = promiseAMapComponent('AmapInfoWindow')
export const AmapPolygon = promiseAMapComponent('AmapPolygon')
export const AmapPolyline = promiseAMapComponent('AmapPolyline')
export const amapApiLoaderInstanceLoad = () => import(/* webpackChunkName: "amap" */ 'vue-amap').then(vam => {
  // vue-amap初始化
  vam.initAMapApiLoader(config)
  // return vam.amapApiLoaderInstanceLoad()
  return vam.lazyAMapApiLoaderInstance.load()
})
