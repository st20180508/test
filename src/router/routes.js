export default [{
  name: '404',
  path: '/404',
  component: require('@/views/error/404')
}, {
  name: '500',
  path: '/500',
    component: require('@/views/error/500')
}, {
  path: '*',
  redirect: '/404'
}, {
  path: '/',
  component: require('@/views/Index'),
  children: [{
    path: '',
    component: require('@/views/DynamicIndex')
  }, {
    path: '/user',
    component: r => require.ensure([], () => r(require('@/views/user/Index')), 'user'),
    children: [{
      path: ':userId',
      component: r => require.ensure([], () => r(require('@/views/user/Main')), 'user'),
      children: [{
        path: '',
        name: 'user',
        component: r => require.ensure([], () => r(require('@/views/user/MainDynamic')), 'user')
      }, {
        path: 'feed/:feedId',
        name: 'userOneFeed',
        component: r => require.ensure([], () => r(require('@/views/user/MainOneDynamic')), 'user')
      }]
    }, {
      path: ':userId/fans',
      name: 'fans',
      component: r => require.ensure([], () => r(require('@/views/user/PersonFans')), 'user')
    }, {
      path: ':userId/follow',
      name: 'follow',
      component: r => require.ensure([], () => r(require('@/views/user/PersonFollow')), 'user')
    }, {
      path: ':userId/favorite',
      name: 'favorite',
      component: r => require.ensure([], () => r(require('@/views/user/PersonFavorite')), 'user')
    }]
  }, {
    path: '/groups',
    name: 'groups',
    component: r => require.ensure([], () => r(require('@/views/groups/Index')), 'group')
  }, {
    path: '/group',
    component: r => require.ensure([], () => r(require('@/views/group/Index')), 'group'),
    children: [{
      path: ':groupId',
      component: r => require.ensure([], () => r(require('@/views/group/Main')), 'group'),
      children: [{
        path: '',
        name: 'group',
        component: r => require.ensure([], () => r(require('@/views/group/MainDynamic')), 'group')
      }, {
        path: 'feed/:feedId',
        name: 'groupOneFeed',
        component: r => require.ensure([], () => r(require('@/views/group/MainOneDynamic')), 'group')
      }, {
        path: 'search',
        name: 'groupSearch',
        component: r => require.ensure([], () => r(require('@/views/search/GroupSearch')), 'group')
      }]
    }, {
      path: ':groupId/member',
      name: 'groupMember',
      component: r => require.ensure([], () => r(require('@/views/group/GroupMember')), 'group')
    }, {
      path: ':groupId/activity',
      name: 'groupActivity',
      component: r => require.ensure([], () => r(require('@/views/group/GroupActivity')), 'group')
    }]
  }, {
    path: '/activity',
    component: r => require.ensure([], () => r(require('@/views/activity/Index')), 'activity')
  }, {
    path: '/activity/:activityId',
    name: 'activity',
    component: r => require.ensure([], () => r(require('@/views/activity/ActivityDetail')), 'activity')
  }, {
    path: '/subscribe',
    name: 'subscribe',
    component: r => require.ensure([], () => r(require('@/views/subscribe/Index')), 'test')
  }, {
    path: '/topic',
    name: 'topic',
    component: r => require.ensure([], () => r(require('@/views/topic/Index')), 'test')
  }, {
    path: '/taskIndex',
    name: 'taskindex',
    component: r => require.ensure([], () => r(require('@/views/task/TaskIndex')), 'test')
  }, {
    path: '/questionIndex',
    name: 'questionindex',
    component: r => require.ensure([], () => r(require('@/views/topic/TopicIndex')), 'test')
  }, {
    path: '/task',
    name: 'task',
    component: r => require.ensure([], () => r(require('@/views/task/Index')), 'test')
  }]
}, {
  path: '/question',
  name: 'question',
  component: r => require.ensure([], () => r(require('@/views/topic/TopicDetail')), 'test')
}, {
  path: '/browse',
  name: 'browse',
  component: r => require.ensure([], () => r(require('@/views/task/TaskDetail')), 'test')
}, {
  path: '/rest',
  name: 'Rest',
  component: r => require.ensure([], () => r(require('@/views/Rest')), 'test')
}, {
  path: '/test',
  name: 'Test',
  component: r => require.ensure([], () => r(require('@/views/Test')), 'test')
}, {
  path: '/sblog',
  component: r => require.ensure([], () => r(require('@/views/SingleNote')), 'test')
}, {
  path: '/config/personal',
  component: r => require.ensure([], () => r(require('@/views/config/personal/Index')), 'personal'),
  children: [{
    path: 'info',
    name: 'info',
    component: r => require.ensure([], () => r(require('@/views/config/personal/BaseInfo')), 'personal')
  }, {
    path: 'security',
    name: 'personalSecurity',
    component: r => require.ensure([], () => r(require('@/views/config/personal/Security')), 'personal')
  }]
}, {
  path: '/config/admin',
  component: r => require.ensure([], () => r(require('@/views/config/admin/Index')), 'admin'),
  children: [{
    path: 'subject',
    name: 'subject',
    component: r => require.ensure([], () => r(require('@/views/config/admin/Subject')), 'admin')
  }, {
    path: 'users',
    name: 'users',
    component: r => require.ensure([], () => r(require('@/views/config/admin/EmployesManager')), 'admin')
  }, {
    path: 'groups',
    name: 'groupsMgr',
    component: r => require.ensure([], () => r(require('@/views/config/admin/GroupsManager')), 'admin')
  }]
}, {
  path: '/post/:id',
  name: 'post',
  component: r => require.ensure([], () => r(require('@/views/include/Post')), 'post')
}, {
  path: '/article/:id',
  name: 'article',
  component: r => require.ensure([], () => r(require('@/views/include/Post')), 'post')
}, {
  path: '/search',
  component: r => require.ensure([], () => r(require('@/views/search/GlobalSearch')), 'search')
}, {
  path: '/news',
  component: r => require.ensure([], () => r(require('@/views/news/Index')), 'news')
}, {
  path: '/nshop',
  component: r => require.ensure([], () => r(require('@/views/nshop/Index')), 'nshop'),
  children: [
    {
      path: '',
      component: r => require.ensure([], () => r(require('@/views/nshop/Main')), 'nshop')
    }, {
      path: '/shopslist/:caId',
      name: 'shopslist',
      component: r => require.ensure([], () => r(require('@/views/nshop/ShopsList')), 'shopslist')
    }, {
      path: '/detail/:arId',
      name: 'detail',
      component: r => require.ensure([], () => r(require('@/views/nshop/ShopsDetail')), 'detail')
    }
  ]
}]
