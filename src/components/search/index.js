/**
 * Created by Administrator on 2017/4/29.
 */
import SearchInput from './src/SearchInput'

SearchInput.install = (Vue) => {
  Vue.component('lg-search', SearchInput)
}

export default SearchInput
