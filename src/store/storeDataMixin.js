import restDataMixin from '@/util/restDataMixin'
import types from '@/store/mutation-types'

export default {
  mixins: [restDataMixin],
  computed: {
    currItem: {
      get () {
        return this.storeState.currItem
      },
      set (val) {
        this.$store.commit(this.storeType.M_CHANGE_CURRENT, val)
      }
    },
    items () {
      return this.storeState.list
    },
    statusDelete () {
      return this.storeState.statusDelete
    },
    statusFetch () {
      return this.storeState.statusFetch
    },
    statusFetchList () {
      return this.storeState.statusFetchList
    },
    statusSave () {
      return this.storeState.statusSave
    },
    page: {
      get () {
        return this.storeState.page.number + 1
      },
      set (val) {
        this.$store.commit(this.storeType.M_CHANGE_PAGE, { page: val - 1 })
      }
    },
    pageInfo () {
      return this.storeState.page
    },
    search: {
      get () {
        return this.storeState.search
      },
      set (val) {
        this.$store.commit(this.storeType.M_SET_SEARCH, val)
      }
    },
    storeType () {
      return types[this.storeModule]
    },
    storeState () {
      return this.$store.state[this.storeModule]
    }
  },
  props: {
    storeModule: {
      type: String,
      required: true
    }
  },
  methods: {
    getData () {
      return this.$store.dispatch(this.storeType.A_FETCH_LIST, this.listUrlParams)
    },
    setCurrItem (val) {
      let {id, item} = this.getItemByVal(val)
      this.$store.commit(this.storeType.M_CHANGE_CURRENT, item)
      return this.$store.dispatch(this.storeType.A_GET_CURRENT, id)
    },
    saveCurrItem (item) {
      return this.$store.dispatch(this.storeType.A_SAVE_CHANGE, {
        oldVal: this.stateCurrItem,
        newVal: item
      })
    },
    restCreate (vo) {
      return this.$store.dispatch(this.storeType.A_CREATE, vo)
    },
    restDelete (id) {
      return this.$store.dispatch(this.storeType.A_DELETE, id)
    },
    restDeleteByIds (ids) {
      return this.$store.dispatch(this.storeType.A_DELETE_IDS, ids)
    },
    restEdit (id, vo) {
      return this.$store.dispatch(this.storeType.A_SAVE, { id, item: vo })
    },
    restGet (id) {
      return this.$store.dispatch(this.storeType.A_GET, id)
    },
    restGetAll (params) {
      return this.$store.dispatch(this.storeType.A_FETCH_LIST, params)
    }
  }
}
