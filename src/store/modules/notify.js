import { buildModule } from '@/store/module'
import { rest } from '@/util'
import qs from 'qs'
import { storeNeedData } from './dataPool'

function getGroupId (msg) {
  if (msg.operObjType === 'GROUP') {
    if (!msg.data) {
      msg.data = {}
    }
    msg.data.groupId = msg.operObjId
  }
  return msg.data && (!msg.data.groupType || msg.data.groupType === 'group') ? msg.data.groupId : null
}

function getObjKey (o) {
  let objKey
  if (o.operObjType === 'POST') {
    objKey = o.operObjType + '!' + o.operObjId
  } else if (o.operObjType === 'COMMENT') {
    if (o.data && o.data.createAtType === '0') { // 评论绑定在帖子上
      objKey = 'POST!' + o.data.createAt
    }
  } else if (o.operObjType === 'IMAGE' || o.operObjType === 'VIDEO' || o.operObjType === 'DOCUMENT') {
    if (o.data && o.data.sourceType === 'POST') {
      objKey = 'POST!' + o.data.sourceId
    }
  }
  if (objKey) {
    o.objKey = objKey
  }
  return objKey
}

const { types, module, options } = buildModule({
  name: 'notify',
  restUrl: '/msg/notifications',
  restListQuery: '',
  types: {
    M_INIT_LIST: 'M_INIT_LIST',
    M_ADD_LIST_TOP: 'M_ADD_LIST_TOP',
    M_SET_UNREAD_COUNT: 'M_SET_UNREAD_COUNT',
    M_MARK_READ: 'M_MARK_READ',
    M_MARK_UNREAD: 'M_MARK_UNREAD',
    A_ADD_LIST_TOP: 'A_ADD_LIST_TOP',
    A_GET_UNREAD_COUNT: 'A_GET_UNREAD_COUN',
    A_MARK_READ: 'A_MARK_READ',
    A_MARK_UNREAD: 'A_MARK_UNREAD'
  },
  state: {
    unreadCount: 0
  },
  afterFetchList (context, req) {
    return req.then(data => {
      if (data.content) {
        return Promise.all([
          storeNeedData([{
            type: 'user',
            dataIds: data.content.map(o => o.fromUser)
          }, {
            type: 'group',
            dataIds: data.content.map(o => getGroupId(o)).filter(o => !!o)
          }], context),
          rest.get('/page/feeds/objs', {
            id: data.content.map(o => getObjKey(o)).filter(o => !!o)
          }, true)
        ])
        .then(([[users, groups], objs]) => {
          let om = objs ? objs.reduce((r, o) => {
            if (o) {
              r[o.objType + '!' + o.objId] = o
            }
            return r
          }, {}) : {}
          data.content.forEach(o => {
            if (o) {
              if (users && o.fromUser) {
                o.user = users[o.fromUser]
              }
              if (groups) {
                let gid = getGroupId(o)
                if (gid) {
                  o.group = groups[gid]
                }
              }
              if (o.objKey) {
                o.feed = om[o.objKey]
              }
            }
          })
          return data
        })
      }
      return data
    })
  }
})

Object.assign(module.mutations, {
  [ types.M_INIT_LIST ] (state, page) {
    state.list = null
    state.page.first = false
    state.page.last = false
    state.page.number = -1
  },
  [ types.M_ADD_LIST_TOP ] (state, vo) {
    if (!state.list) {
      state.list = []
    }
    state.list.splice(0, 0, {...vo})
    state.unreadCount++
  },
  [ types.M_SET_LIST ] (state, page) {
    state.list = (state.list || []).concat(page.content)
    state.page.first = page.first
    state.page.last = page.last
    state.page.size = page.size
    state.page.total = page.totalElements
    state.page.number = page.number
  },
  [ types.M_SET_UNREAD_COUNT ] (state, count) {
    state.unreadCount = count
  },
  [ types.M_MARK_READ ] (state, { all, ids }) {
    if (state.list && (all || ids)) {
      state.list.forEach(m => {
        if (all || ids.indexOf(m.id) >= 0) {
          m.status = 1
        }
      })
    }
  },
  [ types.M_MARK_UNREAD ] (state, ids) {
    if (state.list && ids) {
      state.list.forEach(m => {
        if (ids.indexOf(m.id) >= 0) {
          m.status = 0
        }
      })
    }
  }
})

Object.assign(module.actions, {
  [ types.A_GET_UNREAD_COUNT ] ({ dispatch, commit, state }) {
    return rest.get(options.restUrl + '/unread')
    .then(data => {
      commit(types.M_SET_UNREAD_COUNT, data)
    })
  },
  [ types.A_ADD_LIST_TOP ] (context, msg) {
    let p = [
      storeNeedData([{
        type: 'user',
        dataIds: [msg.fromUser]
      }, {
        type: 'group',
        dataIds: [getGroupId(msg)]
      }], context)
    ]
    let id = getObjKey(msg)
    if (id) {
      p.push(rest.get('/page/feeds/objs', {id}, true))
    }
    return Promise.all(p)
    .then(([[users, groups], objs]) => {
      if (msg.fromUser) {
        msg.user = users[msg.fromUser]
      }
      if (groups) {
        let gid = getGroupId(msg)
        if (gid) {
          msg.group = groups[gid]
        }
      }
      if (objs && objs[0]) {
        msg.feed = objs[0]
      }
      context.commit(types.M_ADD_LIST_TOP, msg)
      return msg
    })
  },
  [ types.A_MARK_READ ] ({ dispatch, commit, state }, { all, ids }) {
    let params
    if (all) {
      params = { all }
    } else if (ids) {
      params = { ids: ids.join(',') }
    }
    if (!params) {
      return
    }
    return rest.edit(options.restUrl + '/mark_read?' + qs.stringify(params))
    .then(data => {
      commit(types.M_SET_UNREAD_COUNT, all || state.unreadCount <= data ? 0 : state.unreadCount - data)
      if (data > 0) {
        commit(types.M_MARK_READ, params)
      }
    })
  },
  [ types.A_MARK_UNREAD ] ({ dispatch, commit, state }, ids) {
    return rest.edit(options.restUrl + '/mark_unread?' + qs.stringify({ids: ids.join(',')}))
    .then(data => {
      commit(types.M_SET_UNREAD_COUNT, (state.unreadCount || 0) + data)
      if (data > 0) {
        commit(types.M_MARK_UNREAD, ids)
      }
    })
  }
})

export { types }
export default module
