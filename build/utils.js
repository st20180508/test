var fs = require('fs')
var path = require('path')
var config = require('../config')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

exports.assetsPath = function (_path) {
  var assetsSubDirectory = process.env.NODE_ENV === 'production'
    ? config.build.assetsSubDirectory
    : config.dev.assetsSubDirectory
  return path.posix.join(assetsSubDirectory, _path)
}

exports.cssLoaders = function (options) {
  options = options || {}

  var cssLoader = {
    loader: 'css-loader',
    options: {
      minimize: process.env.NODE_ENV === 'production',
      sourceMap: options.sourceMap
    }
  }

  // generate loader string to be used with extract text plugin
  function generateLoaders (loader, loaderOptions) {
    var loaders = [cssLoader]
    if (loader) {
      loaders.push({
        loader: loader + '-loader',
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      })
    }

    // Extract CSS when that option is specified
    // (which is the case during production build)
    if (options.extract) {
      return ExtractTextPlugin.extract({
        use: loaders,
        fallback: 'vue-style-loader'
      })
    } else {
      return ['vue-style-loader'].concat(loaders)
    }
  }

  // https://vue-loader.vuejs.org/en/configurations/extract-css.html
  return {
    css: generateLoaders(),
    postcss: generateLoaders(),
    less: generateLoaders('less'),
    sass: generateLoaders('sass', { indentedSyntax: true }),
    scss: generateLoaders('sass'),
    stylus: generateLoaders('stylus'),
    styl: generateLoaders('stylus')
  }
}

// Generate loaders for standalone style files (outside of .vue)
exports.styleLoaders = function (options) {
  var output = []
  var loaders = exports.cssLoaders(options)
  for (var extension in loaders) {
    var loader = loaders[extension]
    output.push({
      test: new RegExp('\\.' + extension + '$'),
      use: loader
    })
  }
  return output
}

// 转换正则特殊字符
exports.regexEscape = function (s) {
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
}

// 获取文件路径
exports.getFiles = function (dir, regex, files_) {
  files_ = files_ || []
  if (!fs.existsSync(dir)) {
    return files_
  }
  var files = fs.readdirSync(dir)
  for (var i in files) {
    var name = dir + '/' + files[i]
    if (fs.statSync(name).isDirectory()) {
      exports.getFiles(name, regex, files_)
    } else if (!regex || regex.test(name)) {
      files_.push(name)
    }
  }
  return files_
}

// 获取多页面实体
exports.getEntries = function (multiPageRoot) {
  if (!multiPageRoot) {
    return []
  }
  var root = path.resolve(__dirname, '../')
  var cut = 0
  if (multiPageRoot.indexOf(root) === 0) {
    cut = root.length
  }
  var fileList = exports.getFiles(multiPageRoot, new RegExp(`^${exports.regexEscape(multiPageRoot)}\/[^/]+\.(js|html)$`))
  var entries = fileList.filter(f => /\.js$/.test(f)).map(f => {
    var name = f.replace(/^.+?([^/]+)\.js$/, '$1')
    return {
      name,
      path: cut > 0 ? '.' + f.substring(cut) : f
    }
  })
  var tmps = fileList.filter(f => /\.html$/.test(f)).reduce((r, f) => {
    var name = f.replace(/^.+?([^/]+)\.html$/, '$1')
    r[name] = f
    return r
  }, {})
  entries.forEach(en => {
    en.filename = en.name + '.html',
    en.template = tmps[en.name] || 'index.html'
  })
  return entries
}
