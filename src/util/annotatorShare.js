import { rest } from '@/util'

export function doAnnTypeFun (ann, type) {
  if (type === 'collect') {
    collectAnnotator(ann)
  } else if (type === 'share') {
    shareAnnotator(ann)
  } else {
  }
}

export function shareAnnotator (ann, annotationTarget, post, callBack) {
  let jsonPost = JSON.parse(JSON.stringify(post))
  let item = {}
  if (!jsonPost.objContent) {
    item.id = jsonPost.feedId
    item.objId = jsonPost.id
    item.objType = 'POST'
    item.groupId = jsonPost.createAt
    item.group = jsonPost.createAtType === 101 ? this.$store.state.dataPool.groups[jsonPost.createAt] : null
    item.scopeGroup = item.group
    item.groupType = jsonPost.createAtType === 101 ? 'group' : 'user'
    item.user = jsonPost.user
    item.objContent = jsonPost
  } else {
    item = jsonPost
  }
  if (callBack) {
    item.callback = callBack
  }
  if (ann && ann.quote && annotationTarget.id) {
    item.colExtContent = ann
  }
  return item
}

export function collectAnnotator (ann, annotationTarget) {
  let preExt = annotationTarget.type.toLowerCase() === 'blog' ? 'BLOG' : 'POST'
  let ext = preExt + '!' + annotationTarget.id
  let sourceType = preExt.toLowerCase()
  if (annotationTarget.type.toLowerCase() === 'document') {
    ext = preExt + '!' + annotationTarget.data.at
    sourceType = annotationTarget.type.toLowerCase()
  }
  let param = {
    type: 'content',
    sourceType: sourceType,
    source: annotationTarget.id,
    ext: ext,
    sourceExtContent: JSON.stringify(ann)
  }
  return rest.create('/sns/favorites', param)
    .then((json) => {
      return json
    })
    .catch(e => {
      if (e && e.status >= 10000 && e.message) {
        this.msgError(e.message)
      } else {
        console.error('收藏帖子标记内容错误！', e)
        this.msgError('收藏帖子标记内容失败，请稍候重试！')
      }
    })
}
