import * as types from './mutation-types'
import { rest } from '@/util'
// import $ from 'jquery'
// import Foundation from 'foundation'

export default {
  [ types.A_GET_CURR_USER ] ({ dispatch, commit, state }) {
    return rest.get('/currUser')
    .then(user => {
      commit(types.M_SET_CURR_USER, { user })
      return user
    })
  }
  // [ types.A_INIT_MEDIA_QUERY ] ({ dispatch, commit, state }, { window }) {
  //   commit(types.M_SET_CURR_MEDIA_QUERY, Foundation.MediaQuery.current)
  //   $(window).on('changed.zf.mediaquery', (event, newSize, oldSize) => {
  //     commit(types.M_SET_CURR_MEDIA_QUERY, newSize)
  //   })
  // }
}
