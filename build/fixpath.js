var fs = require('fs');
var config = require('../config');
var utils = require('./utils')

function replaceContent(filePath, stringToReplace, newString) {
    if (!filePath || !stringToReplace || !newString) {
        return console.log('some parameters are missing');
    }

    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      var re = new RegExp(stringToReplace, 'g');
      var result = data.replace(re, newString);

      fs.writeFile(filePath, result, 'utf8', function (err) {
         if (err) {
            return console.log(err);
         }
      });
    });
}

// Get files of the project
var fileList = utils.getFiles(config.build.assetsRoot, new RegExp(`^${utils.regexEscape(config.build.assetsRoot)}\/[^/]+\.html$`));
// var fileList = [`${config.build.assetsRoot}/index.html`];

fileList.forEach((file) => {
    replaceContent(file, utils.regexEscape('=/static/'), `=${config.build.assetsPublicPath}static/`);
});