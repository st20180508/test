function imgReader (item, imgContent) {
  var [blob, reader] = [item.getAsFile(), new FileReader()]
  var imgContainer = imgContent
  // 读取文件后将其显示在网页中
  reader.onload = function (e, imgContent) {
    var img = new Image()

    img.src = e.target.result
    imgContainer.appendChild(img)
  }
  // 读取文件
  reader.readAsDataURL(blob)
}
function ImgPaste (obj, imgContainer) {
  var _self = obj
  var imgContent = imgContainer
  _self.addEventListener('paste', (e) => {
    // 添加到事件对象中的访问系统剪贴板的接口
    var clipboardData = e.clipboardData
    var {items, item, types} = {}

    if (clipboardData) {
      items = clipboardData.items
      if (!items) return
      item = items[0]
      // 保存在剪贴板中的数据类型
      types = clipboardData.types || []
      const length = types.length
      for (let i = 0; i < length; i++) {
        if (types[i] === 'Files') {
          item = items[i]
          break
        }
      }
      // 判断是否为图片数据
      if (item && item.kind === 'file' && item.type.match(/^image\//i)) {
        imgReader(item, imgContent)
      }
    }
  })
  return this
}
export function imgPaste (obj, imgContainer) {
  return new ImgPaste(obj, imgContainer)
}
