import * as types from './modules'

export const A_GET_CURR_USER = 'root/A_GET_CURR_USER'
export const A_INIT_MEDIA_QUERY = 'root/A_INIT_MEDIA_QUERY'
export const M_SET_CURR_USER = 'root/M_SET_CURR_USER'
export const M_SET_CURR_MEDIA_QUERY = 'root/M_SET_CURR_MEDIA_QUERY'
export const M_SIDEBAR_TOGGER = 'root/M_SIDEBAR_TOGGER'
export const M_SIDEBAR_SMALL_TOGGER = 'root/M_SIDEBAR_SMALL_TOGGER'
export const M_SET_DOCUMENT_HIDDEN = 'root/M_SET_DOCUMENT_HIDDEN'

const moduleTypes = {...types}
delete moduleTypes['default']
export default moduleTypes
