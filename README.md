# saas-web

## js编码规范 
[https://github.com/feross/standard/blob/master/docs/RULES-zhcn.md](https://github.com/feross/standard/blob/master/docs/RULES-zhcn.md)

> saas服务化管理界面

## 编译“饿了么”[Elem](http://element.eleme.io/)样式
```bash
# 初始化全局属性
npm run et -- -i
# 编译css
npm run et
# 实时编译css
npm run et-watch
```

## Build Setup

``` bash
# node 环境安装
# https://nodejs.org/dist/v6.11.0/node-v6.11.0-x64.msi
# https://nodejs.org/dist/v6.11.0/node-v6.11.0-x86.msi
# 升级npm到5.x版本
npm version
npm install -g npm
# install dependencies 进入工程路径

# install dependencies 使用淘宝的镜像
npm install --registry=https://registry.npm.taobao.org --chromedriver_cdnurl=http://npm.taobao.org/mirrors/chromedriver --phantomjs_cdnurl=http://npm.taobao.org/mirrors/phantomjs
# 或者更换 npm 源，命令为 
npm config set registry https://registry.npm.taobao.org

# serve with hot reload at localhost:3000
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## 开发环境设置文件上传用户
```js
// cookie来源于登录环境
window.open((Cookie => 'http://localhost:3000/oauth_callback?headers=' + encodeURIComponent(JSON.stringify({Cookie})))('cookie内容填这里'))
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
