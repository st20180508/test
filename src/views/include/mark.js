  import { rest } from '@/util'
  import { amapApiLoaderInstanceLoad } from '@/components/amap'
  export default {
    data () {
      return {
        showModelForm: false,
        modelFormPlaceholder: '',
        modelFormPrepend: '',
        modelForm: '',
        mapLoaded: false,
        marksUsers: [],
        maxMarksSize: 3,
        searchOption: {
          city: ''
        },
        suggestLoaded: false,
        suggestDelay: 500,
        suggestDelayId: -1,
        noKeywordList: {},
        tagType: {
          marks: [],
          location: []
        },
        objectProps: {
          icon: 'icon',
          name: 'name',
          value: 'value'
        },
        modelData: {}
      }
    },
    watch: {
      'tagType.marks' (nv, ov) {
        this.modelData.marks = nv.map(o => o.id.replace(/[U]?(\w+)/g, '$1'))
        this.storeNeedData({
          type: 'user',
          dataIds: this.modelData.marks
        })
          .then(users => {
            this.marksUsers = []
            for (let id of this.modelData.marks) {
              this.marksUsers.push(users[id])
            }
          })
      },
      'tagType.location' (nv, ov) {
        let location = ''
        if (nv && nv.length) {
          this.modelForm = ''
          this.showModelForm = false
          location = JSON.stringify(nv[0])
        }
        this.modelData.location = location
      }
    },
    computed: {
      _autoComplete () {
        if (!this.mapLoaded) return
        return new window.AMap.Autocomplete(this.searchOption || {})
      },
      getMarksUsers () {
        let count = this.marksUsers.length
        let mus = JSON.parse(JSON.stringify(this.marksUsers))
        if (count > this.maxMarksSize) {
          mus = mus.splice(0, 1)
        }
        return mus
      },
      getOthersMarks () {
        let count = this.marksUsers.length
        let omus = []
        if (count > this.maxMarksSize) {
          omus = JSON.parse(JSON.stringify(this.marksUsers)).splice(1, count)
        }
        return omus.map(o => o.userName).join('<br>')
      }
    },
    methods: {
      toggleModelForm (type, mfp, preTitle) {
        if (type === this.modelForm && this.showModelForm) {
          this.showModelForm = !this.showModelForm
          this.modelForm = ''
          this.modelFormPrepend = ''
          this.modelFormPlaceholder = ''
          return
        } else if (type !== this.modelForm && !this.showModelForm) {
          this.showModelForm = !this.showModelForm
        }
        this.modelForm = type
        this.modelFormPlaceholder = mfp
        this.modelFormPrepend = preTitle
        if (type === 'location') {
          this.objectProps.icon = 'icon'
          this.objectProps.name = 'name'
          this.objectProps.value = 'name'
        } else if (type === 'marks') {
          this.objectProps.icon = 'avatar_c'
          this.objectProps.name = 'name'
          this.objectProps.value = 'id'
        }
        if (type === 'location' && !window.AMap) {
          amapApiLoaderInstanceLoad()
        }
        this.mapLoaded = type === 'location'
      },
      markQuerySearch (queryString, cb) {
        queryString = queryString || ''
        if (this.modelForm === 'location') {
          if (!queryString) {
            cb([])
            return
          }
          this.autoComplete(queryString, cb)
        } else if (this.modelForm === 'marks') {
          this.loadSuggest('user', queryString, cb)
        }
      },
      loadSuggest (type, q, cb) {
        if (this.suggestLoaded) {
          return
        }
        let params = {
          type: type,
          q: q.trim() || '*:*',
          start: 0,
          rows: 10
        }
        if (this.suggestDelayId > -1) {
          clearTimeout(this.suggestDelayId)
          this.suggestDelayId = -1
        }
        this.suggestDelayId = setTimeout(() => {
          this.suggestLoaded = true
          if (params.q === '*:*' && this.noKeywordList && this.noKeywordList[type] && this.noKeywordList[type].length) {
            this.suggestLoaded = false
            cb(this.noKeywordList[type])
          } else {
            rest.get('/sns/search/suggest', params, true)
              .then(data => {
                this.suggestLoaded = false
                if (type === 'group') {
                  let groupList = []
                  for (let group of data) {
                    let item = {
                      id: group.id.replace(/[G]?(\w+)/g, '$1'),
                      value: group.name,
                      icon: group.avatar ? group.avatar_c : require('../../assets/img/1.jpg'),
                      scopeName: this.$store.state.group.groupValueTypes[group.type]
                    }
                    groupList.push(item)
                  }
                  if (params.q === '*:*') {
                    this.noKeywordList[type] = groupList
                  }
                  cb(groupList)
                } else {
                  if (type === 'user') {
                    data.forEach(o => {
                      this.$set(o, 'avatar_c', o.avatar ? o.avatar_c : require('../../assets/img/man.png'))
                    })
                  }
                  if (params.q === '*:*') {
                    this.noKeywordList[type] = data
                  }
                  cb(data)
                }
              })
              .catch(e => {
                this.suggestLoaded = false
                console.log(e)
              })
          }
        }, this.suggestDelay)
      },
      autoComplete (keyword, cb) {
        if (!keyword || !this._autoComplete) return
        this._autoComplete.search(keyword, (status, result) => {
          let restaurants = []
          if (status === 'complete') {
            if (result.tips) {
              result.tips.forEach(o => {
                if (o.location) {
                  let location = {
                    name: o.name,
                    location: o.location.lat + ',' + o.location.lng,
                    lat: o.location.lat,
                    lng: o.location.lng
                  }
                  restaurants.push(location)
                }
              })
            }
          }
          cb(restaurants)
        })
      }
    }
  }
