import { dateYMD, rest } from '@/util'
import restDataMixin from '@/util/restDataMixin'

export default {
  mixins: [restDataMixin],
  props: {
    item: Object,
    itemId: String
  },
  computed: {
    restUrlPrefix () {
      return '/sns/activity'
    },
    initData () {
      return false
    },
    currUser () {
      return this.$store.state.currUser || {}
    },
    group () {
      return this.activity.group
    },
    user () {
      return this.activity.user
    },
    status () {
      let now = Date.now()
      let s
      if (this.activity.status === 1) {
        s = 'cancel'
      } else if (this.activity.status === 2 || this.activity.endDate < now) {
        s = 'done'
      } else if (this.activity.startDate > now) {
        s = 'waiting'
      } else if (this.activity.startDate <= now && this.activity.endDate >= now) {
        s = 'running'
      }
      return s
    },
    isCreator () {
      return this.activity.creator && this.currUser.id === this.activity.creator
    },
    canInvite () {
      return !!this.groupId || (this.activity.scope === 1 && this.activity.invitation && this.activity.mark === 1)
    },
    canJoin () {
      let now = Date.now()
      let limit = this.activity.joinDeadline || this.activity.startDate
      return now < limit && this.activity.status === 0
    },
    joinDeadline () {
      return dateYMD(this.activity.joinDeadline || this.activity.startDate)
    },
    markStr () {
      return this.markList[this.activity.mark]
    },
    endDate () {
      return dateYMD(this.activity.endDate)
    },
    startDate () {
      return dateYMD(this.activity.startDate)
    }
  },
  data () {
    let markList = ['已邀请', '已参加', '有兴趣', '不参加']
    let visitorMap = []
    for (let i = 0; i < markList.length; i++) {
      visitorMap.push({
        key: 'mark' + i,
        label: markList[i],
        list: []
      })
    }
    return {
      saveInviteVisitors: userIds => {
        return rest.create(`/sns/activity/${this.inviteActivity.id}/visitor/invitation`, userIds)
        .then(data => {
          this.selectInviteVisitorDialogVisible = false
          this.msgSuccess('邀请成功')
        })
        .catch(e => {
          this.msgError(e.status < 10000 ? '邀请失败，请稍后重试' : e.message)
        })
      },
      inviteActivity: null,
      markList,
      visitorMap,
      invitedVisitorsPanel: false,
      invitedVisitorsLoading: true,
      invitedActiveName: '',
      selectInviteVisitorDialogVisible: false,
      activity: {}
    }
  },
  watch: {
    $route () {
      if (this.itemId) {
        this.getActivity(this.itemId)
      }
    }
  },
  created () {
    if (this.item) {
      this.activity = this.item
    } else if (this.itemId) {
      this.getActivity(this.itemId)
    }
  },
  methods: {
    getActivity (id) {
      return rest.get('/sns/activity/ids', {ids: [id]}, true)
      .then(([data]) => {
        if (!data) {
          return
        }
        let p = [{
          type: 'user',
          dataIds: [data.creator]
        }]
        if (data.createAt && data.createAtType === 101) { // 小组范围内
          p.push({
            type: 'group',
            dataIds: [data.createAt]
          })
        }
        return this.storeNeedData(p)
        .then(([users, groups]) => {
          data.user = users[data.creator]
          if (groups && data.createAt) {
            data.group = groups[data.createAt]
          }
          this.activity = data
          return this.activity
        })
      })
    },
    showUninvitedVisitorsPanel (item) {
      this.inviteActivity = item
      this.selectInviteVisitorDialogVisible = true
    },
    getInvitedVisitorsForSelectUser () {
      return rest.get(`/sns/activity/${this.inviteActivity.id}/visitor/invitation`)
      .then(data => {
        let nd = data.map(o => ({id: o.visitor, tag: this.markList[o.mark]}))
        nd.push({id: this.inviteActivity.creator, tag: '创建者'})
        return nd
      })
    },
    getVisitors (url) {
      return rest.get(url || `/sns/activity/${this.inviteActivity.id}/visitor/invitation`)
      .then(data => {
        return this.storeNeedData({
          type: 'user',
          dataIds: data.map(o => o.visitor)
        })
        .then(users => {
          this.visitorMap.forEach(o => {
            o.list = []
          })
          data.forEach(o => {
            o.user = users[o.visitor]
            let mark = o.mark < 0 ? 0 : o.mark
            this.visitorMap[mark].list.push(o)
          })
          return data
        })
      })
      .catch(e => {
        this.msgError(e.status < 10000 ? '获取邀请列表失败，请稍后重试' : e.message)
      })
    },
    showInvitedVisitorsPanel (item) {
      this.inviteActivity = item
      this.invitedActiveName = 'mark0'
      this.invitedVisitorsLoading = true
      this.invitedVisitorsPanel = true
      this.getVisitors(`/sns/activity/${item.id}/visitor/invitation`)
      .then(data => {
        this.invitedVisitorsLoading = false
      })
    },
    removeInvitedVisitor (visitor) {
      rest.delete(`/sns/activity/${visitor.activityId}/visitor/invitation?userIds=${visitor.visitor}`)
      .then(data => {
        if (data) {
          this.visitorMap.some(o => {
            let nl = o.list.filter(v => v.id !== visitor.id)
            if (nl.length !== o.list.length) {
              o.list = nl
              return true
            }
          })
        }
      })
      .catch(e => {
        this.msgError(e.status < 10000 ? '移除邀请失败，请稍后重试' : e.message)
      })
    },
    cancelActivity (item) {
      if (item.status === 1) {
        return
      }
      this.$set(item, 'canceling', true)
      return rest.post(`/sns/activity/${item.id}/cancel`)
      .then(data => {
        item.status = 1
        item.canceling = false
      })
      .catch(e => {
        this.msgError(e.message)
        item.canceling = false
      })
    },
    deleteActivity (item) {
      this.$set(item, 'deleting', true)
      return this.restDelete(item.id)
      .then(data => {
        if (data === 1) {
          if (this.rest.items) {
            this.getData()
          } else if (this.activity && this.activity.id) {
            this.$router.push({ path: '/activity' })
          }
        } else {
          item.deleting = false
        }
      })
      .catch(e => {
        this.msgError(e.message)
        item.deleting = false
      })
    },
    editedActivity (item, nv) {
      if (item && nv && Object.keys(nv).length > 0) {
        if (this.rest.items) {
          this.rest.items.some(o => {
            if (o.id === item.id) {
              Object.assign(o, nv)
              return true
            }
            return false
          })
        } else if (this.activity && this.activity.id && this.activity.id === item.id) {
          Object.assign(this.activity, nv)
        }
      }
      item.editing = false
    },
    oper (cmd) {
      this.$emit(cmd, this.activity)
    },
    join (mark) {
      let item = this.activity
      mark = parseInt(mark)
      if (item.mark !== mark) {
        let url = `/sns/activity/${item.id}/visitor`
        let req = item.mark < 0 ? rest.create(url, {mark}) : rest.edit(url + '/mark?mark=' + mark)
        req.then(data => {
          if (item.mark === 1) {
            --item.joinCount
          } else if (mark === 1) {
            this.$set(item, 'joinCount', (item.joinCount || 0) + 1)
          }
          if (item.mark > -1) {
            let ol = this.visitorMap[item.mark].list
            let nl = ol.filter(o => o.visitor !== this.currUser.id)
            if (ol.length !== nl.length) { // 从存在的类别中移除
              this.visitorMap[item.mark].list = nl
            }
          }
          if (mark > -1) {
            let ol = this.visitorMap[mark].list
            let nl = ol.filter(o => o.visitor !== this.currUser.id)
            if (ol.length === nl.length) { // 加入到新类别
              data.user = this.currUser
              ol.push(data)
            }
          }
          this.$emit('join', item, mark)
          item.mark = mark
        })
        .catch(e => {
          if (e.status < 10000) {
            this.msgError(e.message)
            console.error(e)
          } else {
            this.msgWarning(e.message)
          }
        })
      }
    }
  }
}
