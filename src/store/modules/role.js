import { buildModule } from '@/store/module'

const { types, module } = buildModule({
  name: 'role',
  restUrl: '/upms/roles',
  restListQuery: 'roleType=0&sort=roleOrder,desc&sort=id,desc'
})

export { types }
export default module
