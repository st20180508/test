import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
import { basePath } from '@/util'

Vue.use(Router)

export default new Router({
  base: basePath,
  routes,
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
