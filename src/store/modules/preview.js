import { buildModule } from '@/store/module'

const {types, module} = buildModule({
  name: 'preview',
  types: {
    M_CHANGE_LOCATION_DATA: 'M_CHANGE_LOCATION_DATA',
    M_CHANGE_CURRENT_PREVIEW_DATA: 'M_CHANGE_CURRENT_PREVIEW_DATA'
  },
  state: {
    locationData: null,
    previewData: {}
  }
})

Object.assign(module.mutations, {
  [ types.M_CHANGE_LOCATION_DATA ] (state, locationData) {
    state.locationData = locationData
  },
  [ types.M_CHANGE_CURRENT_PREVIEW_DATA ] (state, previewData) {
    state.previewData = previewData
    if (state.locationData) {
      state.previewData.location = state.locationData
      state.locationData = null
    }
  }
})

export {types}
export default module

export const previewData = {
  get () {
    return this.$store.state.preview.previewData
  },
  set (val) {
    this.$store.commit(types.M_CHANGE_CURRENT_PREVIEW_DATA, val)
  }
}
