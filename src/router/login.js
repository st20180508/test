import Vue from 'vue'
import Router from 'vue-router'
import { basePath } from '@/util'

Vue.use(Router)

const routes = [{
  path: '/login',
  component: require('@/views/login/Login')
}]

export default new Router({
  base: basePath,
  routes,
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
